import test from 'tape';
import { inspectArgsFromContent, getCommand } from '../../src/Base/Commands';
import { commands } from '../../src/Base/State';
import { createDiscordCommand } from '../TestUtilities/Stubs';

test('inspectArgsFromContent', t => {
    const helpCommand = inspectArgsFromContent('>>help input', '>>');

    t.deepEqual(helpCommand.args, ['input'], 'An argument were provided.');
    t.strictEqual(helpCommand.command, 'help', 'The "command" field is correct.');
    t.deepEqual(helpCommand.flags, {
        _: ['input']
    }, 'The "flags" field is empty.');
    t.strictEqual(helpCommand.input, 'input', 'The "input" field is correct.');
    t.strictEqual(helpCommand.base, 'help input', 'The "base" field is correct.');
    t.deepEqual(helpCommand.split, ['help', 'input'], 'The "split" field is correct.');

    t.end();
});

test('getCommand', t => {
    const stubCommand = createDiscordCommand();
    commands.set('x', stubCommand);
    t.strictEqual(getCommand('x'), stubCommand, 'It reads from the "commands" map.');
    commands.delete('x');
});

test('command', t => {
    const stubCommand = createDiscordCommand();

});
