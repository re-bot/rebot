import { DiscordCommand, BaseDiscordCommand } from "../../src/Base/Types";

/** Creates an empty function cast to a BaseDiscordCommand. */
export const createBaseDiscordCommand = () => 
    (() => {}) as BaseDiscordCommand;

/** Creates a DiscordCommand. */
export const createDiscordCommand = () => 
    Object.assign(() => {}, {
        modName: 'modName',
        path: 'path',
        category: 'category'
    }) as DiscordCommand;
