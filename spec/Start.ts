import 'dotenv/config';
import '@babel/register';
import { readFileSync } from 'fs';
import { resolve } from 'path';
import moduleAlias from 'module-alias';
import chalk from 'chalk';

const babelrcPath = resolve(__dirname, '../.babelrc');
const babelrc = JSON.parse(readFileSync(babelrcPath, 'utf8'));

// Try to find the options for the "module-resolver" Babel plugins in the .babelrc, so require() still works.
let [, moduleResolverOpts] = babelrc.plugins.find((x: unknown[]) => x[0] === 'module-resolver');

if (!moduleResolverOpts) {
    console.warn(`Cannot find Babel "module-resolver" plugin configuration.`);
    moduleResolverOpts = {};
}

let { alias } = moduleResolverOpts;

if (!alias) {
    console.warn(`Could not get "alias" field in the configuration for the "module-resolver" Babel plugin.`);
    alias = {};
} else {
    alias = Object.keys(alias).reduce((acc, curr) => {
        const value = alias[curr];
        return {
            ...acc,
            [curr]: resolve(__dirname, '../', value)
        }
    }, {});
}

moduleAlias.addAliases(alias);

const message = chalk.green('Initiated testing environment.');
console.log(message);

import './Base/Commands.spec';
