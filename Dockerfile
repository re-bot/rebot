FROM node

# RUN apk add --no-cache git
# RUN apk add --no-cache openssh

RUN ["mkdir", "/rebot", "/rebot/src"]
COPY package.json /rebot/
COPY babel.config.js /rebot/
COPY tsconfig.json /rebot/
COPY src/. /rebot/src/
COPY scripts/. /rebot/scripts/
WORKDIR /rebot

RUN npm i && npm i -D && \
  rm -rf ./node_modules/hastebin/index.d.ts && \
  npm run build

FROM node:carbon-alpine
WORKDIR rebot
COPY --from=0 /rebot/. ./
CMD ["npm", "start"]