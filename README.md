<!-- markdownlint-disable MD033 MD031 MD041 -->

<div align="center">
    <h1>Rebot</h1>
    A small, easy-to-use and configurable Discord (and soon to be Riot) bot. Also comes with a range of commands for everything.
</div>

---

```txt
             ( Rebot is a small, functional, configurable and extendable Discord bot. )
   |\---/|  O
   | ,_, | o
    \_`_/-..----.
 ___/ `   ' ,""+ \  sk
(__...'   __\    |`.___.';
  (_,...'(_,.`__)/'.....+
```

## How to install Rebot

You can install Rebot using `git`.

```sh
git clone https://gitlab.com/rebot-discord/rebot.git
cd rebot
```

Then, it's just a matter of setting up `npm`.

```sh
npm install

# Only use this if you want to develop Rebot
npm install --save-dev
```

## Configuration

To configure Rebot, you'll need to add some things to a `.env` file. If you don't read this, Rebot is super nice and will warn you ;)

### Setting up your `.env` file

Copy and paste this into a file called `.env`.

```env
# The production discord token.
DISCORD_TOKEN=

# The token for the development account, which is used on "npm run start:dev".
DISCORD_DEV_TOKEN=

# The command prefix for the production account.
DISCORD_PREFIX=<>

# The prefix for the production account.
DISCORD_DEV_PREFIX=<<
```

### Extending Rebot

Rebot is super configurable too, and it has some cool development warnings and logs, so code away. If TypeScript isn't your fancy, I'll be adding macros soon in an easier language for non-programmers. And no, it's not going to be a visual Scratch monster.

## What's so good about Rebot

1. Look above. Rebot is very extensible, and programming it should be easy.

2. Rebot comes with a range of commands from NSFW commands to simple Reddit / RSS ones.

3. The default CLI for Rebot is crafted to be friendly to people that forget something, or people that are playing with it. It's just meant to be friendly to developers. If you can improve this, send an issue in this Git repository.

## Configuring PostgreSQL

For the database to function correctly, you'll need to setup PostgreSQL.

1. Make sure PostgreSQL is installed.
2. Set it up for Rebot, like so:
    ```sql
    CREATE ROLE rebot WITH PASSWORD 'your password here';
    CREATE DATABASE rebot OWNER rebot;
    ALTER ROLE rebot with LOGIN;
    ```

3. And make sure your `pg_hba.conf` includes this, so Rebot can login:
    ```py
    # "local" is for Unix domain socket connections only
    local all all md5
    ```

## Questions

If you have any questions, join my [Discord server](https://discord.gg/rJr7qqq).

```txt
           __..--''``---....___   _..._    __
 /// //_.-'    .-/";  `        ``<._  ``.''_ `. / // /
///_.-' _..--.'_    \                    `( ) ) // //
/ (_..-' // (< _     ;_..__               ; `' / ///
 / // // //  `-._,_)' // / ``--...____..-' /// / //
```

The cats are from [here](https://www.asciiart.eu/animals/cats). If you like these ascii figures, check out the `ascii` command.
