const prompts = require('prompts');
const { existsSync, readFileSync, writeFileSync } = require('fs');
const partial = require('lodash/partial');
const has = require('lodash/has');
const flow = require('lodash/flow');
const { resolve } = require('path');
const { parse } = require('dotenv');
const envPath = resolve(__dirname, '../.env');
const envExists = existsSync(envPath);
const getEnv = flow(
	partial(readFileSync, envPath),
	parse
);

const fields = {
	DISCORD_TOKEN: "What's your Discord token for the production account of this bot?",
	DISCORD_DEV_TOKEN: "what's your Discord token for the development account of this bot?",
	DISCORD_PREFIX: 'What do you want the prefix to be?',
	DISCORD_DEV_PREFIX: 'What do you want the prefix to be for the development account?'
};

const initialFieldValues = {
	DISCORD_PREFIX: '**',
	DISCORD_DEV_PREFIX: '**>'
};

const fieldNames = Object.keys(fields);
const missingFields = [];

if (envExists) {
	const object = getEnv();
	missingFields.concat(fieldNames.filter(name => !has(object, name)));
} else {
	missingFields.concat(fieldNames);
}

const promptsInput = missingFields.map(fieldName => ({
	type: fieldName.includes('TOKEN') ? 'hidden' : 'text',
	name: fieldName,
	message: fields[fieldName],
	initial: initialFieldValues[fieldName]
}));

console.log(missingFields);
console.log(promptsInput);

(async () => {
	const env = await prompts(promptsInput);
})();
