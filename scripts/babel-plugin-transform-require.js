function transformRequirePlugin (babel) {
    return {
        visitor: {
            CallExpression (expression) {
                // If it's a "require(...)" call...
                if (expression.callee && expression.callee.name === 'require') {
                    console.log(expression.callee.arguments);
                }

                if (expression.callee && expression.callee.type === 'Identifier' && expression.callee.name === 'require') {
                    const moduleName = expression.arguments[0];
                    console.log({ moduleName });
                    expression.replaceWith({
                        ...expression,
                        arguments: [
                            moduleName.replace(/\.ts$/i, ''),
                            // Support other arguments just in case Node adds support for some reason.
                            expression.arguments.slice(1)
                        ]
                    });
                }
            }
        }
    }
}

module.exports = transformRequirePlugin;
