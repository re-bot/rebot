/* eslint-disable filenames/match-regex */
module.exports = {
	extends: ['@commitlint/config-conventional'],
	rules: {
		'scope-enum': [2, 'always', ['bot', 'cli', 'command', 'types']],
        'subject-case': [2, 'always', 'lower-case'],
		'type-enum': [
			2,
			'always',
			['build', 'ci', 'docs', 'feat', 'fix', 'perf', 'refactor', 'revert', 'style', 'test', 'chore']
		]
	}
};
