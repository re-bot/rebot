const fs = require('fs');
const filesInSrc = fs.readdirSync('./src/').map(name => name.replace(/\.ts$/, ''));
const path = require('path');
const babelPluginTransformRequire = require('./scripts/babel-plugin-transform-require');

module.exports = {
	"presets": [
		"@babel/preset-env",
		["@babel/preset-typescript"]
	],
	"plugins": [
		"macros",
		"lodash",
		"@babel/plugin-proposal-optional-chaining",
		"@babel/plugin-proposal-nullish-coalescing-operator",
		["@babel/plugin-transform-typescript", { "allowNamespaces": true }],
		["@babel/plugin-proposal-decorators", { "legacy": true }],
		[
			"module-resolver",
			{
				"root": ["./src/"],
				// Map the folder names to an object which is essentially...
				"alias": filesInSrc.reduce((collection, folderName) => {
					// This extends the collection with (example) { 'constants': './src/constants' }
					collection[path.parse(folderName).name] = `./${path.join('./src/', folderName)}`;
					return collection;
				}, {})
			}
		],
		["version", {
			"define": "__version__",
			"stringLiteral": true
		}],
		["@comandeer/babel-plugin-banner", {
			"banner": "/*! This code is generated. Do not edit it directly. */\n"
		}],
		"transform-typescript-metadata",
		["@babel/plugin-proposal-class-properties", { "loose": true }],
		babelPluginTransformRequire
	],
	"sourceMaps": true
}