module.exports = {
	env: {
		es6: true,
		node: true
	},
	extends: ['eslint:recommended', 'plugin:@typescript-eslint/eslint-recommended', 'strict'],
	globals: {
		Atomics: 'readonly',
		SharedArrayBuffer: 'readonly',
		Intl: 'readonly'
	},
	parser: '@typescript-eslint/parser',
	parserOptions: {
		ecmaVersion: 2018,
		sourceType: 'module'
	},
	plugins: ['@typescript-eslint', 'prettier'],
	rules: {
		indent: ['error', 'tab'],
		'linebreak-style': ['error', 'unix'],
		quotes: ['error', 'single'],
		semi: ['error', 'always'],
		'no-tabs': ['off', { allowIndentationTabs: true }],
		'filenames/match-regex': [2, '^[a-z][a-z1-9_-]+$', true],
		'no-unused-vars': 'off',
		'@typescript-eslint/no-unused-vars': ['error', { vars: 'all', args: 'after-used', ignoreRestSiblings: false }],
		'@typescript-eslint/array-type': ['error', { default: 'array-simple' }],
		'@typescript-eslint/generic-type-naming': ['error', '^[A-Z][a-zA-Z]+$'],
		'@typescript-eslint/prefer-function-type': ['error'],
		'prettier/prettier': 'error',
		'array-bracket-spacing': ['error', 'never'],
		'array-element-newline': ['error', 'consistent'],
		'comma-dangle': ['error', 'never'],
		'func-style': ['error', 'declaration'],
		'template-curly-spacing': ['error', 'never'],
		'no-trailing-spaces': ['error', {
			skipBlankLines: true
		}],
		'no-floating-promises': ['error', {
			ignoreVoid: true
		}]
	}
};
