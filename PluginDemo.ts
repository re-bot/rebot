/* eslint-disable filenames/match-regex */

// @ts-ignore
import { client } from '@rebot/state';
// @ts-ignore
import { onDiscordMessage, onBootstrap, onShutdown, replyTo, logger, onShardStart, onShardShutdown } from '@rebot/plugins';
import { Message } from 'eris';

onDiscordMessage((message: Message) => {
    if (message.content === 'pong') {
        replyTo(message, 'ping!');
    }
});

onBootstrap(() => {
    logger.log(`you can hook into anything Rebotty with the public plugin api...`);
});

onShutdown(() => {
    logger.log('even shutdown events, anything really...');
});

onShardStart((shard) => {
    if (shard.id === 2) {
        // yay! my favourite shard has awoken
    }

    logger.log('hopefully the plugins will be managed by a CLI via readline');
});

setTimeout(() => {
    const linuxCafe = client.guilds.find(x => x.name === 'Linux Cafe');

    if (linuxCafe) {
        const newsChannel = linuxCafe.channels.find(channel => channel.name === 'news');

        if (!newsChannel) {
            return;
        }

        newsChannel.createMessage('post an embed here or something');
    }
});
