import { nekosLifeClient } from "state";
import { createNekosLifeImageCommand } from "utilities/discord";

export default createNekosLifeImageCommand (() => nekosLifeClient.nsfw.cumsluts(), {
    usage: 'cumsluts',
    description: 'Get a cumslut image / GIF. Fetched from the https://nekos.life API.',
    nsfw: true
});
