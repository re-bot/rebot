import { nekosLifeClient } from "state";
import { createNekosLifeImageCommand } from "utilities/discord";

export default createNekosLifeImageCommand (() => nekosLifeClient.nsfw.boobs(), {
    usage: 'boobs',
    description: 'Get a boobs image / GIF. Fetched from the https://nekos.life API.',
    nsfw: true
});
