import { nekosLifeClient } from "state";
import { createNekosLifeImageCommand } from "utilities/discord";

export default createNekosLifeImageCommand (() => nekosLifeClient.nsfw.randomHentaiGif(), {
    usage: 'random-hentai',
    description: 'Get a random hentai GIF. Fetched from the https://nekos.life API.',
    nsfw: true
});
