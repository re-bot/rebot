import { nekosLifeClient } from "state";
import { createNekosLifeImageCommand } from "utilities/discord";

export default createNekosLifeImageCommand (() => nekosLifeClient.nsfw.eroFeet(), {
    usage: 'ero-feet',
    description: 'Get a ero-feet image / GIF. Fetched from the https://nekos.life API.',
    nsfw: true
});
