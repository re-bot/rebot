import { nekosLifeClient } from "state";
import { createNekosLifeImageCommand } from "utilities/discord";

export default createNekosLifeImageCommand (() => nekosLifeClient.nsfw.trap(), {
    usage: 'trap',
    description: 'Get a trap image / GIF. Fetched from the https://nekos.life API.',
    nsfw: true
});
