import { nekosLifeClient } from "state";
import { createNekosLifeImageCommand } from "utilities/discord";

export default createNekosLifeImageCommand (() => nekosLifeClient.nsfw.neko(), {
    usage: 'neko',
    description: 'Get a neko image. Fetched from the https://nekos.life API.',
    nsfw: true
});
