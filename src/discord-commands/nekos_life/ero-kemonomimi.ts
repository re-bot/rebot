import { nekosLifeClient } from "state";
import { createNekosLifeImageCommand } from "utilities/discord";

export default createNekosLifeImageCommand (() => nekosLifeClient.nsfw.eroKemonomimi(), {
    usage: 'ero-kemonomimi',
    description: 'Get a ero kemonomimi image / GIF. Fetched from the https://nekos.life API.',
    nsfw: true
});
