import { nekosLifeClient } from "state";
import { createNekosLifeImageCommand } from "utilities/discord";

export default createNekosLifeImageCommand (() => nekosLifeClient.nsfw.tits(), {
    usage: 'tits',
    description: 'Get a tits image / GIF. Fetched from the https://nekos.life API.',
    nsfw: true
});
