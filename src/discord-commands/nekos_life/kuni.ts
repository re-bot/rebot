import { nekosLifeClient } from "state";
import { createNekosLifeImageCommand } from "utilities/discord";

export default createNekosLifeImageCommand (() => nekosLifeClient.nsfw.kuni(), {
    usage: 'kuni',
    description: 'Get a kuni image / GIF. Fetched from the https://nekos.life API.',
    nsfw: true
});
