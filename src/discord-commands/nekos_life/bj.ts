import { nekosLifeClient } from "state";
import { createNekosLifeImageCommand } from "utilities/discord";

export default createNekosLifeImageCommand (() => nekosLifeClient.nsfw.bJ(), {
    usage: 'bj',
    description: 'Get a bj image / GIF. Fetched from the https://nekos.life API.',
    nsfw: true
});
