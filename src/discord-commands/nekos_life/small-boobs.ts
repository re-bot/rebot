import { nekosLifeClient } from "state";
import { createNekosLifeImageCommand } from "utilities/discord";

export default createNekosLifeImageCommand (() => nekosLifeClient.nsfw.smallBoobs(), {
    usage: 'small-boobs',
    description: 'Get an image / GIF of small boobs. Fetched from the https://nekos.life API.',
    nsfw: true
});
