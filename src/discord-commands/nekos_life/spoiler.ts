import { nekosLifeClient } from "state";
import { createNekosLifeImageCommand } from "utilities/discord";

export default createNekosLifeImageCommand (() => nekosLifeClient.sfw.holo(), {
    usage: 'holo',
    description: 'Get a Holo image / GIF. Fetched from the https://nekos.life API.'
});
