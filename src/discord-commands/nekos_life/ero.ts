import { nekosLifeClient } from "state";
import { createNekosLifeImageCommand } from "utilities/discord";

export default createNekosLifeImageCommand (() => nekosLifeClient.nsfw.ero(), {
    usage: 'ero',
    description: 'Get a ero image / GIF. Fetched from the https://nekos.life API.',
    nsfw: true
});
