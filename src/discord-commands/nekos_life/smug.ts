import { nekosLifeClient } from "state";
import { createNekosLifeImageCommand } from "utilities/discord";

export default createNekosLifeImageCommand (() => nekosLifeClient.sfw.smug(), {
    usage: 'smug',
    description: 'Get a smug image / GIF. Fetched from the https://nekos.life API.'
});
