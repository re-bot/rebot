import { command } from "utilities/commands";
import { send, escapeCodeBlock } from "utilities/messages";
import { nekosLifeClient } from "state";

export default command(async (message, inspected) => {
    const { input: text } = inspected;

    const { owo } = await nekosLifeClient.sfw.OwOify({
        text
    });
    send(`> ${escape(owo)}`, message);
}, {
    usage: 'owoify',
    description: '"OwOifies" your message. Fetched from the https://nekos.life API.'
});