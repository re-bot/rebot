import { nekosLifeClient } from "state";
import { createNekosLifeImageCommand } from "utilities/discord";

export default createNekosLifeImageCommand (() => nekosLifeClient.sfw.baka(), {
    usage: 'baka',
    description: 'Get a baka image / GIF. Fetched from the https://nekos.life API.'
});
