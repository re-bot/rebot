import { nekosLifeClient } from "state";
import { createNekosLifeImageCommand } from "utilities/discord";

export default createNekosLifeImageCommand (() => nekosLifeClient.nsfw.hentai(), {
    usage: 'hentai',
    description: 'Get a hentai image / GIF. Fetched from the https://nekos.life API.',
    nsfw: true
});
