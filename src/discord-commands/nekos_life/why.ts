import { command } from "utilities/commands";
import { send } from "utilities/messages";
import { nekosLifeClient } from "state";

export default command(async (message) => {
    const { why } = await nekosLifeClient.sfw.why();
    send(`> ${why}`, message);
}, {
    usage: 'why',
    description: 'Gets a random "why" question. Fetched from the https://nekos.life API.'
});