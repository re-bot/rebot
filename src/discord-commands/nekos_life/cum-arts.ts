import { nekosLifeClient } from "state";
import { createNekosLifeImageCommand } from "utilities/discord";

export default createNekosLifeImageCommand (() => nekosLifeClient.nsfw.cumArts(), {
    usage: 'cum-arts',
    description: 'Get a cum arts image / GIF. Fetched from the https://nekos.life API.',
    nsfw: true
});
