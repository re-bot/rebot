import { nekosLifeClient } from "state";
import { createNekosLifeImageCommand } from "utilities/discord";

export default createNekosLifeImageCommand (() => nekosLifeClient.sfw.kemonomimi(), {
    usage: 'kemonomimi',
    description: 'Get a kemonomimi image / GIF. Fetched from the https://nekos.life API.',
    nsfw: true
});
