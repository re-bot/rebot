import { nekosLifeClient } from "state";
import { createNekosLifeImageCommand } from "utilities/discord";

export default createNekosLifeImageCommand (() => nekosLifeClient.nsfw.pussyArt(), {
    usage: 'pussy-art',
    description: 'Get an image / GIF of pussy art. Fetched from the https://nekos.life API.',
    nsfw: true
});
