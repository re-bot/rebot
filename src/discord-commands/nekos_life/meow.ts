import { nekosLifeClient } from "state";
import { createNekosLifeImageCommand } from "utilities/discord";

export default createNekosLifeImageCommand (() => nekosLifeClient.sfw.meow(), {
    usage: 'meow',
    description: 'Get a meow image / GIF. Fetched from the https://nekos.life API.'
});
