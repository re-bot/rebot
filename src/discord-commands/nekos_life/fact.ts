import { command } from "utilities/commands";
import { send, escapeCodeBlock } from "utilities/messages";
import { nekosLifeClient } from "state";

export default command(async (message, inspected) => {
    const { fact } = await nekosLifeClient.sfw.fact();

    send(`> ${escape(fact)}`, message);
}, {
    usage: 'fact',
    description: 'Gets a random fact. Fetched from the https://nekos.life API.'
});