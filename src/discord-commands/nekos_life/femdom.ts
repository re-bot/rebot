import { nekosLifeClient } from "state";
import { createNekosLifeImageCommand } from "utilities/discord";

export default createNekosLifeImageCommand (() => nekosLifeClient.nsfw.femdom(), {
    usage: 'femdom',
    description: 'Get a femdom image / GIF. Fetched from the https://nekos.life API.',
    nsfw: true
});
