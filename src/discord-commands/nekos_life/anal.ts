import { nekosLifeClient } from "state";
import { createNekosLifeImageCommand } from "utilities/discord";

export default createNekosLifeImageCommand (() => nekosLifeClient.nsfw.anal(), {
    usage: 'anal',
    description: 'Get a anal image / GIF. Fetched from the https://nekos.life API.',
    nsfw: true
});
