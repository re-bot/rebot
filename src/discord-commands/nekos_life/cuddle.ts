import { nekosLifeClient } from "state";
import { createNekosLifeImageCommand } from "utilities/discord";

export default createNekosLifeImageCommand (() => nekosLifeClient.sfw.cuddle(), {
    usage: 'cuddle',
    description: 'Get a cuddle image / GIF. Fetched from the https://nekos.life API.'
});
