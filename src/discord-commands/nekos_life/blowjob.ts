import { nekosLifeClient } from "state";
import { createNekosLifeImageCommand } from "utilities/discord";

export default createNekosLifeImageCommand (() => nekosLifeClient.nsfw.blowJob(), {
    usage: 'blowjob',
    description: 'Get a blowjob image / GIF. Fetched from the https://nekos.life API.',
    nsfw: true
});
