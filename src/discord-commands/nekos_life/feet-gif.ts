import { nekosLifeClient } from "state";
import { createNekosLifeImageCommand } from "utilities/discord";

export default createNekosLifeImageCommand (() => nekosLifeClient.nsfw.feetGif(), {
    usage: 'feet-gif',
    description: 'Get a feet GIF. Fetched from the https://nekos.life API.',
    nsfw: true
});
