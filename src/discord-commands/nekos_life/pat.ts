import { nekosLifeClient } from "state";
import { createNekosLifeImageCommand } from "utilities/discord";

export default createNekosLifeImageCommand (() => nekosLifeClient.sfw.pat(), {
    usage: 'pat',
    description: 'Get a pat image / GIF. Fetched from the https://nekos.life API.'
});
