import { command } from "utilities/commands";
import { send, escapeCodeBlock } from "utilities/messages";
import { nekosLifeClient } from "state";

export default command(async (message) => {
    const { cat } = await nekosLifeClient.sfw.catText();
    send(`\`${escapeCodeBlock(cat)}\``, message);
}, {
    usage: 'cat',
    description: 'Gets a random ascii cat. Fetched from the https://nekos.life API.'
});