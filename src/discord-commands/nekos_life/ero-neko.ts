import { nekosLifeClient } from "state";
import { createNekosLifeImageCommand } from "utilities/discord";

export default createNekosLifeImageCommand (() => nekosLifeClient.nsfw.eroNeko(), {
    usage: 'ero-neko',
    description: 'Get a ero neko kemonomimi image / GIF. Fetched from the https://nekos.life API.',
    nsfw: true
});
