import { nekosLifeClient } from "state";
import { createNekosLifeImageCommand } from "utilities/discord";

export default createNekosLifeImageCommand (() => nekosLifeClient.nsfw.girlSoloGif(), {
    usage: 'girl-solo-gif',
    description: 'Get an GIF of a "solo girl". Fetched from the https://nekos.life API.',
    nsfw: true
});
