import { nekosLifeClient } from "state";
import { createNekosLifeImageCommand } from "utilities/discord";

export default createNekosLifeImageCommand (() => nekosLifeClient.sfw.kiss(), {
    usage: 'kiss',
    description: 'Get a kiss image / GIF. Fetched from the https://nekos.life API.'
});
