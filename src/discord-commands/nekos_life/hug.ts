import { nekosLifeClient } from "state";
import { createNekosLifeImageCommand } from "utilities/discord";

export default createNekosLifeImageCommand (() => nekosLifeClient.sfw.hug(), {
    usage: 'hug',
    description: 'Get a hug image / GIF. Fetched from the https://nekos.life API.'
});
