import { nekosLifeClient } from "state";
import { createNekosLifeImageCommand } from "utilities/discord";

export default createNekosLifeImageCommand (() => nekosLifeClient.nsfw.yuri(), {
    usage: 'yuri',
    description: 'Get a yuri image / GIF. Fetched from the https://nekos.life API.',
    nsfw: true
});
