import { nekosLifeClient } from "state";
import { createNekosLifeImageCommand } from "utilities/discord";

export default createNekosLifeImageCommand (() => nekosLifeClient.nsfw.keta(), {
    usage: 'keta',
    description: 'Get an image / GIF of a keta. Fetched from the https://nekos.life API.',
    nsfw: true
});
