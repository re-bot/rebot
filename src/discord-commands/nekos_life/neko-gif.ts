import { nekosLifeClient } from "state";
import { createNekosLifeImageCommand } from "utilities/discord";

export default createNekosLifeImageCommand (() => nekosLifeClient.nsfw.nekoGif(), {
    usage: 'neko-gif',
    description: 'Get a neko GIF. Fetched from the https://nekos.life API.',
    nsfw: true
});
