import { nekosLifeClient } from "state";
import { createNekosLifeImageCommand } from "utilities/discord";

export default createNekosLifeImageCommand (() => nekosLifeClient.nsfw.feet(), {
    usage: 'feet',
    description: 'Get a feet image / GIF. Fetched from the https://nekos.life API.',
    nsfw: true
});
