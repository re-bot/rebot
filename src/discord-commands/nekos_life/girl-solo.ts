import { nekosLifeClient } from "state";
import { createNekosLifeImageCommand } from "utilities/discord";

export default createNekosLifeImageCommand (() => nekosLifeClient.nsfw.girlSolo(), {
    usage: 'girl-solo',
    description: 'Get an image of a "solo girl". Fetched from the https://nekos.life API.',
    nsfw: true
});
