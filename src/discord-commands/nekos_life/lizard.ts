import { nekosLifeClient } from "state";
import { createNekosLifeImageCommand } from "utilities/discord";

export default createNekosLifeImageCommand (() => nekosLifeClient.sfw.lizard(), {
    usage: 'lizard',
    description: 'Get a lizard image / GIF. Fetched from the https://nekos.life API.'
});
