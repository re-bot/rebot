import { nekosLifeClient } from "state";
import { createNekosLifeImageCommand } from "utilities/discord";

export default createNekosLifeImageCommand (() => nekosLifeClient.sfw.foxGirl(), {
    usage: 'fox-girl',
    description: 'Get a fox-girl image / GIF. Fetched from the https://nekos.life API.'
});
