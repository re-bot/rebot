import { nekosLifeClient } from "state";
import { createNekosLifeImageCommand } from "utilities/discord";

export default createNekosLifeImageCommand (() => nekosLifeClient.nsfw.eroYuri(), {
    usage: 'ero-yuri',
    description: 'Get a ero yuri image / GIF. Fetched from the https://nekos.life API.',
    nsfw: true
});
