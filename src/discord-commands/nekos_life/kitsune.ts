import { nekosLifeClient } from "state";
import { createNekosLifeImageCommand } from "utilities/discord";

export default createNekosLifeImageCommand (() => nekosLifeClient.nsfw.kitsune(), {
    usage: 'kitsune',
    description: 'Get an image / GIF of a kitsune. Fetched from the https://nekos.life API.',
    nsfw: true
});
