import { nekosLifeClient } from "state";
import { createNekosLifeImageCommand } from "utilities/discord";

export default createNekosLifeImageCommand (() => nekosLifeClient.sfw.woof(), {
    usage: 'woof',
    description: 'Get a dog image / GIF. Fetched from the https://nekos.life API.'
});
