import { nekosLifeClient } from "state";
import { createNekosLifeImageCommand } from "utilities/discord";

export default createNekosLifeImageCommand (() => nekosLifeClient.sfw.poke(), {
    usage: 'poke',
    description: 'Get a poke image / GIF. Fetched from the https://nekos.life API.'
});
