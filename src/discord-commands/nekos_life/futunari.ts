import { nekosLifeClient } from "state";
import { createNekosLifeImageCommand } from "utilities/discord";

export default createNekosLifeImageCommand (() => nekosLifeClient.nsfw.futanari(), {
    usage: 'futunari',
    description: 'Get a futunari image / GIF. Fetched from the https://nekos.life API.',
    nsfw: true
});
