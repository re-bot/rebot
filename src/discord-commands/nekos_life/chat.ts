import { command } from "utilities/commands";
import { send, escapeCodeBlock } from "utilities/messages";
import { nekosLifeClient } from "state";

export default command(async (message, inspected) => {
    const { input: text } = inspected;

    const { response } = await nekosLifeClient.sfw.chat({
        text
    });
    send(`> ${escape(response)}`, message);
}, {
    usage: 'chat',
    description: 'Chats with an AI (Artificial Intelligence) for chatting. Fetched from the https://nekos.life API.'
});