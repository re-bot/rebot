import { nekosLifeClient } from "state";
import { createNekosLifeImageCommand } from "utilities/discord";

export default createNekosLifeImageCommand (() => nekosLifeClient.sfw.slap(), {
    usage: 'slap',
    description: 'Get a slap image / GIF. Fetched from the https://nekos.life API.'
});
