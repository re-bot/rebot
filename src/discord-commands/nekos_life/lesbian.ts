import { nekosLifeClient } from "state";
import { createNekosLifeImageCommand } from "utilities/discord";

export default createNekosLifeImageCommand (() => nekosLifeClient.nsfw.lesbian(), {
    usage: 'lesbian',
    description: 'Get a lesbian image / GIF. Fetched from the https://nekos.life API.',
    nsfw: true
});
