import { nekosLifeClient } from "state";
import { createNekosLifeImageCommand } from "utilities/discord";

export default createNekosLifeImageCommand (() => nekosLifeClient.nsfw.pussyGif(), {
    usage: 'pussy-gif',
    description: 'Get a pussy GIF. Fetched from the https://nekos.life API.',
    nsfw: true
});
