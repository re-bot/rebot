import { nekosLifeClient } from "state";
import { createNekosLifeImageCommand } from "utilities/discord";

export default createNekosLifeImageCommand (() => nekosLifeClient.nsfw.eroKitsune(), {
    usage: 'ero-kitsune',
    description: 'Get a ero kitsune image / GIF. Fetched from the https://nekos.life API.',
    nsfw: true
});
