import { nekosLifeClient } from "state";
import { createNekosLifeImageCommand } from "utilities/discord";

export default createNekosLifeImageCommand (() => nekosLifeClient.sfw.feed(), {
    usage: 'feed',
    description: 'Get a feeding image / GIF. Fetched from the https://nekos.life API.'
});
