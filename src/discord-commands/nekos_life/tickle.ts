import { nekosLifeClient } from "state";
import { createNekosLifeImageCommand } from "utilities/discord";

export default createNekosLifeImageCommand (() => nekosLifeClient.sfw.tickle(), {
    usage: 'tickle',
    description: 'Get a tickle image / GIF. Fetched from the https://nekos.life API.'
});
