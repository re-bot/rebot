import { nekosLifeClient } from "state";
import { createNekosLifeImageCommand } from "utilities/discord";

export default createNekosLifeImageCommand (() => nekosLifeClient.nsfw.holoEro(), {
    usage: 'holo-ero',
    description: 'Get a Holo ero image / GIF. Fetched from the https://nekos.life API.',
    nsfw: true
});
