/** @prettier */

import figlet from 'figlet';
import { command } from 'utilities/commands';
import { Message } from 'eris';
import { InspectedArguments, YargsParser } from 'all-types';
import { isFigletFont } from 'utilities/figlet';

export default command(
	async (message: Message, inspected: InspectedArguments, flags: YargsParser.Arguments) => {
		const [fontName] = inspected.args;
		const font = isFigletFont(fontName) ? fontName.replace(/[_-]/g, ' ') : 'Small';

		// eslint-disable-next-line no-sync
		const output = figlet.textSync(inspected.args.slice(1).join(' '), { ...flags, font } as any);
		message.channel.createMessage(`\`\`\`${output}\`\`\``);
	},
	{
		description: 'Returns an ASCII version of the input text.',
		usage: 'ascii font input'
	}
);
