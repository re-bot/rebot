import { command } from "utilities/commands";
// @ts-ignore
import { createPaste } from 'hastebin';
import { Base64 } from 'js-base64';
import { send } from "utilities/messages";
import { commands } from "state";

export default command((message) => {
    const subCommandNames = Object.keys(commands.get('encode')?.subCommands ?? {});
    send(`Try one of the subcommands: \n${subCommandNames.map(x => `\`${x}\``).join(', ')}.`, message);
}, {
    description: 'This is an empty command. Use one of the subcommands.'
});

export const base64 = command(async (message, inspected, flags) => {
    const encoded = Base64.encode(inspected.input);
    
    try {
        const url = await createPaste(encoded, { raw: flags.raw });
        await send(`Done. ${url}`, message);
    } catch (error) {
        // TODO: add error logging for this
        send('Sorry. I couldn\'t upload the encoded version to Hastebin.', message);
    }
}, {
    flags: {
        boolean: ['raw']
    },
    description: 'Encodes a string to base64.',
    usage: 'base64 input'
});

export const hex = command((message) => {

}); 

export {
    base64 as b64
}

