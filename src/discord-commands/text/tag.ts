/** @prettier */

import { command } from 'utilities/commands';
import { Tag } from 'utilities/database/models';
import { ResolvableColorName } from 'all-types';
import { GuildChannel, Guild } from 'eris';
import { send } from 'utilities/messages';

export const get = command(async (message, inspected) => {
	const [name] = inspected.args;
	const guild: Guild | void = (message.channel as GuildChannel).guild;
	const foundTag = await Tag.findOne({ name, guildId: guild.id });

	if (name === undefined) {
		message.channel.createMessage('Did you forget to pass me the name of the tag you want to view?');
		return;
	}

	if (foundTag) {
		message.channel.createMessage({
			embed: {
				description: foundTag.content,
				timestamp: foundTag.createdAt,
				color: ResolvableColorName.Blue
			}
		});
	} else {
		// TODO: sanitize "name"
		message.channel.createMessage(`I couldn't find a tag named \`${name}\`.`);
	}
});

// "get" is an alias for the base command.
export default get;

export const set = command(async (message, inspected, flags) => {
	const { input: baseInput, args } = inspected;
	const { author } = message;
	const guild: Guild | void = (message.channel as GuildChannel).guild;

	if (args.length < 2) {
		send('Expected at least two arguments. Did you forget to include the content?', message);
		return;
	}

	// if (flags.global && !(await userHasRebotPermission(author.id, 'global-tags'))) {
	// 	send(`You don't have permission to create global tags.`, message);
	// 	return;
	// }

	const [name] = args;

	const content = baseInput.slice(name.length + 1);
	const descriptor = {
		name,
		content,
		authorID: author.id,
		guildId: guild.id
	};
	
	const alreadyExists = (await Tag.findOne(descriptor)) !== undefined;

	if (alreadyExists) {
		send(`Sorry, but the \`${name}\` tag already exists. Choose another name.`, message);
		return;
	}

	const newTag = Tag.create(descriptor);

	try {
		await newTag.save();
		send('Done.', message);
	} catch (saveError) {
		send(`Sorry. The \`${name}\` cannot be set.`, message);
		// TODO: add error logging for this
	}
}, {
	flags: {
		alias: {
			g: 'global'
		},
		boolean: ['global']
	}
});

export const info = command(async (message, inspected) => {
	const [name] = inspected.args;
	const guild: Guild | void = (message.channel as GuildChannel).guild;

	if (name === undefined) {
		message.channel.createMessage('You forgot to pass the name of the tag.');
		return;
	}

	const tag = await Tag.find({ name, guildId: guild.id });

	if (tag) {
		message.channel.createMessage({
			embed: {
				title: name,
				description: ''
			}
		});
	} else {
		message.channel.createMessage(`Cannot find information for tag \`${name}\`, as it doesn't exist.`);
	}
});

export const remove = command(async (message, inspected) => {
	const [name] = inspected.args;

	if (name === undefined) {
		// TODO: move this message into a json file / message storage file
		message.channel.createMessage('You forgot to pass the name of the tag.');
		return;
	}

	const guild: Guild | void = (message.channel as GuildChannel).guild;
	const tag = await Tag.findOne({ name, guildId: guild.id });

	if (!tag) {
		// TODO: move this message into a json file / message storage file
		message.channel.createMessage(`I couldn't find a tag named \`${name}\`.`);
		return;
	}

	try {
		await tag.remove();
		// TODO: move this message into a json file / message storage file
		message.channel.createMessage('Done.');
	} catch (removeError) {
		message.channel.createMessage(`Sorry. I couldn't remove the \`${name}\` tag.`);
		// TODO: log this error
	}
});

export { remove as delete, info as lookup };
