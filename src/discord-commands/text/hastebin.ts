import { command } from 'utilities/commands';
// @ts-ignore
import hastebin from 'hastebin';

export default command(
	async (message, inspected, flags) => {
		try {
			const url = await hastebin.createPaste(inspected.input, {
				raw: flags.raw ?? false
			});

			message.channel.createMessage(`Your paste can be found here: ${url}`);
		} catch (uploadError) {
			message.channel.createMessage('Failed to create a paste.');
			// TODO: add error logging for this
		}
	},
	{
		description: 'Uploads your text to hastebin as a paste.',
		usage: 'hastebin',
		flags: {
			boolean: ['raw'],
			alias: {
				r: 'raw'
			}
		}
	}
);
