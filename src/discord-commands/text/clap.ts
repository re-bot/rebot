import { command } from "utilities/commands";
import { send } from "utilities/messages";

export default command ((message, inspected) => {
    const { input } = inspected;
    // TODO: move regex tor regex constants
    // TODO: move this emoji to emoji constants
    const clapped = input.replace(/[^:]\w+[^:\b]/g, letter => `:clap: ${letter}`);

    return send(clapped, message);
});