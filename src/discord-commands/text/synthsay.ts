import { command } from "utilities/commands";
import { send } from "utilities/messages";

const map: Record<string, string> = 
    ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
    .reduce((object, letter) => {
        // Spaces are required so the emojis don't bump into eachother and smash.
        object[letter] = `:regional_indicator_${letter}:`;
        return object;
    }, {} as Record<string, string>);

export default command((message, inspected) => {
    const { input } = inspected;
    const formatted = input
        .replace(/[\w\s]/g, (match) => match === ' ' ? ' :black_small_square: ' : map[match.toLowerCase()])
        .replace(/\w+(\s+)\w+/g, ' :black_small_square: ');

    return send(formatted, message);
});