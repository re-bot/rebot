/** @prettier */

import { command } from 'utilities/commands';
import { GuildChannel } from 'eris';
import { Source } from 'utilities/source';

export default command(
	message => {
		const { guild } = message.channel as GuildChannel;
		const { latency } = guild.shard;

		message.channel.createMessage(`Pong! :ping_pong: **${latency}ms**.`);
	},
	{
		sources: [Source.Guild],
		description: 'Gets the latency for this shard.',
		usage: 'ping'
	}
);
