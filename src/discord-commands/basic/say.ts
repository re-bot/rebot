/** @prettier */

import { command } from 'utilities/commands';
import { TextChannel, GuildChannel } from 'eris';
import * as Coercion from 'utilities/yargs-coercion';
import { owners } from 'r-constants';
import { send } from 'utilities/messages';

export default command(
	(message, inspected, flags) => {
		const { author, channel } = message;
		const { guild } = channel as GuildChannel;
		const baseContent = flags._.join(' ');

		const channelID = flags.channel as string;
		const requestedChannel = guild.channels.find(channel => channel.id === channelID) as TextChannel;

		if (!requestedChannel) {
			message.channel.createMessage("I can't find that channel.");
			return;
		}

		let content = baseContent;

		// If they aren't an owner, we don't want people thinking the bot is saying it.
		if (!owners.includes(author.id)) {
			content = `*<@${author.id}> says...\n${baseContent
				.split('\n')
				.map(line => `> ${line}`)
				.join('\n')}`;
		}

		send(content, requestedChannel);
	},
	{
		flags: {
			alias: {
				c: 'channel'
			},
			coerce: {
				channel: Coercion.mention
			}
		}
	}
);
