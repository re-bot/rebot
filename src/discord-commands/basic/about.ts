import { command } from "utilities/commands";
import { send } from "utilities/messages";
import { aboutEmbed } from "r-constants";

export default command((message) => {
    send({
        embed: aboutEmbed
    }, message);
}, {
    usage: 'about',
    description: 'Tells you more about Rebot.'
});
