/** @prettier */

import { command, commandLoader } from 'utilities/commands';
import { Message, GuildChannel, Guild } from 'eris';
import { InspectedArguments, DiscordCommand } from 'all-types';
import { prefix, owners } from 'r-constants';
import { testPermissions } from 'utilities/discord';
import { send } from 'utilities/messages';
import { stripIndent } from 'common-tags';

/** The separator for embeds that list help items. */
const separator = ', ';

export default command(
	(message: Message, { args }: InspectedArguments) => {
		const categories = Array.from(commandLoader.categories.values());
		const commandNames = Array.from(commandLoader.store.keys());
		const commands = Array.from(commandLoader.store.values());
		const { author } = message;  
		const guild: Guild | void= (message.channel as GuildChannel).guild;

		if (args.length > 0) {
			if (args.length > 1) {
				message.channel.createMessage('You can only select one command.');
				return;
			}

			const [name] = args;

			// Is it a category?
			const isCategory = categories.includes(name);
			// or a Command?
			const isCommand = !isCategory && commandNames.includes(name);

			if (isCategory) {
				const matchingCommands = 
				commands.filter(x => x.category === name).filter(x => {
					// If the command is from a guild, check the user's permissions in the guild.
					if (!guild || testPermissions(x.permissions ?? {}, guild.members.find(x => x.id === author.id))) {
						return true;
					}

					// Remove owner commands if the author is not an owner.
					if (!x.permissions || (x.permissions.isOwnerCommand && owners.includes(author.id))) {
						return true;
					};

					return false;
				});

				// TODO: add limit to length as guard
				message.channel.createMessage({
					embed: {
						title: `Commands in the ${name} category. Try \`${prefix}help\` for help with individual commands.`,
						description: matchingCommands.map(x => x.modName).join(separator)
					}
				});
			} else if (isCommand) {
				const command = commandLoader.store.get(name) as DiscordCommand;
				const subCommandNames = Object.keys(command.subCommands);
				const hasAnySubCommands = subCommandNames.length !== 0;
				const userCanUseCommand = command.permissions ? testPermissions(command.permissions, guild.members.get(author.id)!) : false;
				const isOwnerCommand = command.permissions?.isOwnerCommand ?? false;
				message.channel.createMessage({
					embed: {
						title: name,
						description: stripIndent `${command.description}
						
						**Usage**: \`\`\`${command.usage}\`\`\`
						${hasAnySubCommands ? `\n**Subcommands**: ${subCommandNames.map(escape).join(', ')}` : ''}

						${command.nsfw ? '**This command is NSFW. It can only be used in a NSFW channel.**' : ''}
						${isOwnerCommand ? '**This command can only be used by the owners of this bot.**' : ''}
						${(!isOwnerCommand && !userCanUseCommand) ? '**You do not have permission to use this command.**' : '' /* We don't want to show the owner notice twice. */}
						`
					}
				});
			} else {
				message.channel.createMessage("I'm not sure what that is. Try entering a command / category name.");
			}
		} else {
			// TODO: add limit to length as guard
			send({
				embed: {
					title: `Try \`${prefix}help\` for one of these categories.`,
					description: categories.join(separator)
				}
			}, message);
		}
	},
	{
		usage: 'help [command | category]',
		description: 'Retrieves help for a certain command, category, or prints a list of categories.'
	}
);
