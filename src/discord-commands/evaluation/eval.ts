/** @prettier */

import { Message } from 'eris';
import { command, commandLoader } from 'utilities/commands';
import { InspectedArguments } from 'all-types';
import { createContext, runInContext } from 'vm';
import { inspect } from 'util';
import execa from 'execa';
import flow from 'lodash/flow';
import { client } from 'state';
import { escapeCodeBlock } from 'utilities/messages';

const display = flow([inspect, escapeCodeBlock]);

// @ts-ignore
export default command(
	async (message: Message, inspected: InspectedArguments) => {
		const { input } = inspected;

		try {
			const context = createContext({
				message,
				inspected,
				input,
				execa,
				flow,
				inspect,
				Message,
				process,
				global,
				require,
				client,
				commandLoader
			});

			const result = runInContext(input, context);

			if (result instanceof Promise) {
				const placeholder = await message.channel.createMessage('```Waiting for Promise to resolve...```');
				const value = await result;
				placeholder.edit(`\`\`\`Promise<${display(value)}>\`\`\``);
			} else {
				const escaped = display(result);
				message.channel.createMessage(`\`\`\`${escaped}\`\`\``);
			}
		} catch (evalError) {
			message.channel.createMessage(`Error :warning: \`\`\`${evalError}\`\`\``);
		}
	},
	{
		permissions: {
			isOwnerCommand: true
		},
		usage: 'eval [expression]',
		description: 'Evaluates a JavaScript expression.'
	}
);
