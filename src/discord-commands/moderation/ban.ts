/** @prettier */

import { command } from 'utilities/commands';
import { reUserMention } from 'r-constants';
import { Source } from 'utilities/source';
import { send } from 'utilities/messages';
import { OperationResult } from 'all-types';
import { isAllowedToBan } from 'utilities/discord';
import toMilliseconds from 'ms';
import clamp from 'lodash/clamp';
import { GuildChannel } from 'eris';

export default command(
	async (message, inspected, flags) => {
		const { guild } = message.channel as GuildChannel;

		// prettier-ignore
		const userIDs: Array<string | undefined> = flags._.map(id => reUserMention.exec(id)?.[1]);
		const usersToBan = userIDs.map((id) => guild.members.find(member => member.id === id));
		// TODO: optimize this
		const invalidIDs = userIDs.filter((id) => !guild.members.find(member => member.id === id));

		if (invalidIDs.length > 0) {
			send(
				`Cannot find ${invalidIDs.map((invalidID) => `\`${invalidID}\``).join(', ')}`,
				message,
				OperationResult.Failed
			);
			return;
		}

		const unbannableMembers = usersToBan.filter((member) => isAllowedToBan(member));

		if (unbannableMembers.length > 0) {
			send(
				`Cannot ban \`${unbannableMembers.map((member) => `\`${member.id}\``)}`,
				message,
				OperationResult.AccessDenied
			);
			return;
		}

		const reason = flags.reason?.join(' ');
		const baseDeleteMessageDays: string = flags.deleteMessageDays?.join(' ');
		const deleteMessageDays = baseDeleteMessageDays ? clamp(toMilliseconds(baseDeleteMessageDays), 7) : undefined;

		if (!deleteMessageDays) {
			send(`Invalid time: \`${baseDeleteMessageDays}\`.`, message, OperationResult.Failed);
			return;
		}

		for (const user of usersToBan) {
			await guild.banMember(user.id, deleteMessageDays, reason);
		}

		send('Done. It was about time.', message);
	},
	{
		flags: {
			configuration: {
				'camel-case-expansion': true
			},
			alias: {
				time: 'purge',
				date: 'purge',
				length: 'purge',
				deleteMessageDays: 'purge'
			},
			array: ['reason', 'purge']
		},
		sources: [Source.Guild]
	}
);
