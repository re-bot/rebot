/** @prettier */

import { command } from 'utilities/commands';
import { reUserMention } from 'r-constants';
import { Source } from 'utilities/source';
import { send } from 'utilities/messages';
import { OperationResult } from 'all-types';
import { isAllowedToBan } from 'utilities/discord';
import toMilliseconds from 'ms';
import clamp from 'lodash/clamp';
import { GuildChannel } from 'eris';

export default command(
	async (message, inspected, flags) => {
		const { guild } = message.channel as GuildChannel;

		// prettier-ignore
		const userIDs: Array<string | undefined> = flags._.map(id => reUserMention.exec(id)?.[1]);
		const usersToKick = userIDs.map((id) => guild.members.find(member => member.id === id));
		// TODO: optimize this
		const invalidIDs = userIDs.filter((id) => !guild.members.find(member => member.id === id));

		if (invalidIDs.length > 0) {
			send(
				`Cannot find ${invalidIDs.map((invalidID) => `\`${invalidID}\``).join(', ')}`,
				message,
				OperationResult.Failed
			);
			return;
		}

		const unbannableMembers = usersToKick.filter((member) => isAllowedToBan(member));

		if (unbannableMembers.length > 0) {
			send(
				`Cannot kick \`${unbannableMembers.map((member) => `\`${member.id}\``)}`,
				message,
				OperationResult.AccessDenied
			);
			return;
		}

		const reason = flags.reason?.join(' ');

		for (const user of usersToKick) {
			await guild.kickMember(user.id, reason);
		}

		send('Done. It was about time.', message);
	},
	{
		flags: {
			configuration: {
				'camel-case-expansion': true
			},
			array: ['reason']
		},
		sources: [Source.Guild]
	}
);
