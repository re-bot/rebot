import { command } from "utilities/commands";
import { mention, FlagCoercionError } from "utilities/yargs-coercion";
import { GuildChannel } from "eris";

export default command ((message, inspected) => {
    const [selectedPerson] = inspected.args;
    const { author, channel } = message;
    const { guild } = channel as GuildChannel;

    if (selectedPerson === undefined) {
        message.channel.createMessage(`You have to choose someone to spank. uwu`);
        return;
    }

    const selectedID = mention(selectedPerson);
    const selectedMember = guild.members.find(member => member.id === selectedID);

    if (selectedID instanceof FlagCoercionError || !selectedPerson) {
        message.channel.createMessage(`You have to spank someone that *exists*. uwu`);
        return;
    }

    message.channel.createMessage(`${author.mention} spanks ${selectedMember.mention}. That felt good.`);
}, {
    usage: 'spank (user-id|user-mention)'
});