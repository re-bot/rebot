import prettyMs from 'pretty-ms';
import { command } from 'utilities/commands';
import { send } from 'utilities/messages';

export default command((message, inspected, flags) => {
    const { input: baseInput } = inspected;
    // Remove any leading / trailing spaces, so the below regex check doesn't freak out about it.
    const input = baseInput.trim();

    if (input.length === 0) {
        return send('You need to pass a number to this command.', message);
    }

    const parsedInput = parseFloat(input);

    if (!parsedInput) {
        return send(`That's not a valid number. Try again?`, message);
    }

    const output = prettyMs(parsedInput, {
        unitCount: flags.u,
        verbose: flags.v,
        compact: flags.c,
        keepDecimalsOnWholeSeconds: flags.k,
        millisecondsDecimalDigits: flags.m,
        secondsDecimalDigits: flags.s,
        separateMilliseconds: flags.S,
        formatSubMilliseconds: flags.F
    });
    return send(`\`${output}\``, message);
}, {
    usage: 'pretty-ms ms',
    description: 'Transforms milliseconds into a human-readable date.',
    flags: {
        boolean: ['verbose', 'compact', 'keep-decimals-on-whole-seconds', 'separate-milliseconds', 'format-sub-milliseconds'],
        alias: {
            verbose: ['v', 'l', 'long'],
            compact: ['c', 'short'],
            'keep-decimals-on-whole-seconds': ['k', 'keep-decimals'],
            'unit-count': ['u'],
            'milliseconds-decimal-digits': ['m'],
            'seconds-decimal-digits': ['s'],
            'separate-milliseconds': ['S'],
            'format-sub-milliseconds': ['F']
        },
        number: ['seconds-decimal-digits', 'milliseconds-decimal-digits', 'unit-count']
    }
});
