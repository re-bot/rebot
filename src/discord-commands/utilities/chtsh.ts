import { command } from "utilities/commands";
import fetch from 'got';
import { send, escapeCodeBlock } from "utilities/messages";
import { ResolvableColorName } from "all-types";
import { logError } from "utilities/logging";
import { stripIndent } from 'common-tags';
import { ansiEscapeCodeRe } from "r-constants";

export default command(async (message, inspected) => {
    const { input } = inspected;

    try {
        const { body: response } = await fetch(`https://cht.sh/${input}`, {
            headers: {
                'User-Agent': 'curl/7.37.0'
            }
        });

        // This api doesn't send 404s
        if (response.startsWith('Unknown topic.')) {
            return send({
                embed: {
                    title: `Could not find cheatsheet for \`${input}\``,
                    description: `That \`cht.sh\` page cannot be found.\nPerhaps you meant one of these? \`\`\`${response.split('\n').slice(3).join('\n').replace(/^\s+/mg, '')}\`\`\``,
                    color: ResolvableColorName.Red
                }
            }, message);
        }

        send({
            embed: {
                title: `Cheat sheet for \`${input}\``,
                description: `\`\`\`${escapeCodeBlock(response.replace(ansiEscapeCodeRe, ''))}\`\`\``,
                color: ResolvableColorName.Blue
            }
        }, message);
    } catch (httpError) {
            // TODO: make these fucking error embeds a constant
            await send({
                embed: {
                    title: 'Sorry. I couldn\'t do that.',
                    description: 'Something went wrong here. An error, if you will. Resynth is on the case!',
                    color: ResolvableColorName.Red
                }
            }, message);

            logError(httpError);
    }
});
