import { command } from "utilities/commands";
import { RebotGuild } from "utilities/database/models";
import { GuildChannel } from "eris";
import { Source } from "utilities/source";
import { send } from "utilities/messages";
import { GuildSettingsMap, guildSettingsPrettyNames } from "r-constants";
import { stripIndent } from 'common-tags';

export default command(async (message, inspected) => {
    const { guild } = (message.channel as GuildChannel);

    try {
        const descriptor = { id: guild.id };
        const guildEntity = await RebotGuild.findOne(descriptor) ?? RebotGuild.create(descriptor);
        // We know this key is valid JSON, so we don't need to wrap the ".parse" call in a "try" block.
        // Provide a default value, as the guild may not yet be in the database (see the above "??").
        const settings: GuildSettingsMap = JSON.parse(guildEntity.settings ?? '{}');

        /* Send an embed that shows the user the configuration for the current guild. */
        send({
            embed: {
                title: `Here's the configuration for this guild.`,
                description: `\`\`\`${Object.keys(settings).map(key => {
                    const prettyName = guildSettingsPrettyNames[key];
                    return `${prettyName}: ${(settings as any)[key]}`;
                }).join('\n')}\`\`\``
            }
        }, message);
    } catch (error) {
        // TODO: add error logging for this
        // TODO: make decent error messages
        send(`An error happened. Sorry.`, message);
    }
}, {
    sources: [Source.Guild],
    permissions: {
        nodes: ['MANAGE_GUILD']
    }
});