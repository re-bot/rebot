import { command } from "utilities/commands";
import { client } from "state";
import { suggestionsWebhookID, suggestionsWebhookToken } from "r-constants";
import { send } from "utilities/messages";
import { ResolvableColorName } from 'all-types';

export default command (async (message, inspected) => {
    const { input } = inspected;
    const { author } = message;
    
    try {
        await client.executeWebhook(suggestionsWebhookID, suggestionsWebhookToken, {
            embeds: [{
                title: `New suggestion from **${author.username}#${author.discriminator}**`,
                description: input,
		color: ResolvableColorName.Blue,
            }]
        });
        await send(`Thanks. Your suggestion has been sent to Resynth :)`, message);
    } catch (error) {
        // TODO: add error logging for this, and the other 1000000 things we need error logging for
        await send('I couldn\'t do that. Can you try again later?', message);
    }
});
