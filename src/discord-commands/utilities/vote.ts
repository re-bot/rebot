import { command } from "utilities/commands";
import { wordsToNumbers, numbersToWords } from "r-constants";
import { range, hasOwnProperty, sleep } from "utilities/general-util";
import { GuildChannel, Guild, TextChannel } from "eris";
import { mention } from "utilities/yargs-coercion";
import { send } from "utilities/messages";
import { ChannelType } from "r-constants";
import { client } from "state";
import { Poll } from "utilities/database/models";
import { stripIndent } from 'common-tags';

// TODO move this to emoji constants
const numberEmojis: Record<number, string> = {
    1: '1⃣',
    2: '2⃣',
    3: '3⃣',
    4: '4⃣',
    5: '5⃣',
    6: '6⃣',
    7: '7⃣',
    8: '8⃣',
    9: '9⃣',
    10: '🔟'
};

export default command(async (message, inspected, flags) => {
    /** An array of arguments that are not bound to a flag. */
    const loneArguments = flags._;
    // Guild isn't marked as "| void" here, because we check for it below.
    const guild: Guild | void = (message.channel as GuildChannel).guild;
    const { author } = message;

    if (loneArguments.length > 1) {
        // The user might want to get a poll with that id.
        const [pollId] = loneArguments;
        
        try {
            const poll = await Poll.findOne({ messageId: pollId });

            if (poll) {
                try {
                    const pollMessage = await client.getMessage(poll.messageId, poll.channelId);
                    const pollGuild: Guild | void = (pollMessage.channel as GuildChannel).guild;

                    // Some basic security: Don't send the information if the poll is in a different guild, or if it's a direct message not from this user.
                    if (pollGuild ? pollGuild.id !== guild.id : pollMessage.author.id !== author.id) {
                        send(`I couldn't find that poll.`, message);
                        return;                        
                    }

                    await send({ embed: {
                        title: `Here's the information for that poll.`,
                    description: stripIndent `It's in the ${pollMessage.channel.mention} channel. You can view it here: https://discordapp.com/channels/${pollMessage.channel.id}/${pollMessage.id}
                    It has ${poll.choices.length} choices. It was created by ${message.author.mention}.` }}, message);
                } catch (error) {
                    // -- Something happened in one of the promises above, maybe the "getMessage" call threw an error.
                    // todo: add error logging for this
                    await send(`Sorry. Something went wrong while searching for that poll.`, message);
                    return;
                }
            } else {
                // We actually couldn't find the poll. It's probably invalid.
                send(`I couldn't find that poll.`, message);
                return;  
            }
        } catch (error) {
            // -- The database "findOne" call threw an error.
            // TODO: add error logging for this
            await send(`Sorry. Something went wrong while searching for that poll.`, message);
            return;
        }
    }

    let amountOfQuestions = 0;
    const questions = Object.keys(flags)
        // Remove all unneeded flags.
        .filter(name => hasOwnProperty(wordsToNumbers, name))
        // Reduce it down to an object.
        .reduce((object, name) => {
            object[name] = flags[name].join(' ');
            // Increment the amount variable. This will save us doing another `Object.keys` call (performance).
            ++amountOfQuestions;
            return object;
        }, {} as Record<string, string>);

    const selectedChannelID = flags.channel ?? message.channel.id;

    if (amountOfQuestions > 10) {
        await send(`You can only have 10 questions.`, message);
        return;
    }

    if (!guild && selectedChannelID) {
        await send(`You can't choose a channel while using this from Direct Messages.`, message);
        return;
    }

    /** The channel the user has selected. */
    const selectedChannel = guild.channels.find(channel => channel.id === selectedChannelID) as TextChannel;

    if (selectedChannel.type !== ChannelType.GuildText || !selectedChannel) {
        await send(`The channel you have selected is not a text channel.`, message);
        return;
    }

    // Now we need to construct the final message we're going to send.
    let finalMessage = '';
    let reactionsToAdd: string[] = [];

    let numberEmojisIndex = 1;
    // For each question...
    for (const key of Object.keys(questions)) {
        // Get the corresponding emoji for this index.
        const correspondingEmoji = numberEmojis[numberEmojisIndex];
        const isLastIndex = numberEmojisIndex === amountOfQuestions;

        finalMessage += `${correspondingEmoji} ${questions[key]}${isLastIndex ? '' : '\n'}`;
        // Remove the colons from the emoji name, as I'm pretty sure Eris doesn't like it.
        reactionsToAdd.push(correspondingEmoji);

        ++numberEmojisIndex;
    }

    try {
    /** The final question message. */
    const questionMessage = await send(finalMessage, selectedChannel ?? message.channel);

    // Add the reactions to the message.
    for (const baseReaction of reactionsToAdd) {
        // Transform the few code points emoji to a one code point emoji (I don't know either).
        const reaction = [baseReaction][0];
        await questionMessage.addReaction(reaction);
    }

    try {
        const pollItem = Poll.create({
            authorId: author.id,
            choices: Object.values(questions),
            messageId: questionMessage.id,
            channelId: selectedChannelID
        });

        await pollItem.save();
    } catch (saveError) {
        // TODO: add error logging for this
        await send(`Sorry. I couldn't save this poll.`, message);
        return;
    }

    // And that's a wrap!
    await send(`Done.`, message);
    } catch (sendError) {
        // TODO: add error logging for this
        await send(`Sorry. I couldn't create the poll.`, message);
        return;
    }
}, {
    usage: 'vote --one Are cats better than dogs? --two Are dogs better than cats? --timeout 2h --channel #general',
    description: 'Starts a poll / voting session. You can also use timeouts, see the above example. The maximum amount of number flags (-1, -2 etc.) is 10.',
    flags: {
        coerce: {
            channel: mention
        },
        alias: {
            t: 'timeout',
            c: 'channel',
            // ...Object.keys(wordsToNumbers).reduce((acc, curr) => ({ ...acc, [curr]: wordsToNumbers[curr] }), {})
        },
        array: [
            'one',
            'two',
            'three',
            'four',
            'five',
            'six',
            'seven',
            'eight',
            'nine',
            'ten'
        ]
    }
});


