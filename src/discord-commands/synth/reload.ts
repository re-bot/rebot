/** @prettier */

import { command, commandLoader } from 'utilities/commands';
import { Message } from 'eris';
import { InspectedArguments } from 'all-types';
import Logger from 'utilities/logging';

const logger = Logger.get('reloadcmd');

export default command(
	async (message: Message, inspected: InspectedArguments) => {
		const { args: commandsToReload } = inspected;
		const outputMessage = await message.channel.createMessage(`Reloading \`${commandsToReload.join(', ')}\`... `);
		const failed: string[] = [];

		for (const command of commandsToReload) {
			try {
				await commandLoader.reloadByName(command);
			} catch (reloadError) {
				// fixme: log the error
				failed.push(command);
			}
		}

		const errors = failed.length !== 0;

		const output = errors
			? `Reloaded, but with errors. \`${failed.join(', ')}\` failed to load.`
			: `Reloaded \`${commandsToReload.join(', ')}\`.`;
		outputMessage.edit(output);
	},
	{
		permissions: {
			isOwnerCommand: true
		},
		usage: 'reload command',
		description: 'Reloads a command, unloading and loading it into the command registry.'
	}
);
