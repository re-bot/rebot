import { command } from "utilities/commands";
import { say } from 'cowsay';
import { boolean } from "utilities/yargs-coercion";
import { send, escapeCodeBlock } from "utilities/messages";

export default command((message, inspected, flags) => {
    const text = flags._.join(' ');
    const theCowSays = say({
        text,
        eyes: flags.eyes,
        tongue: flags.tongue,
        wrap: flags.wrap,
        wrapLength: isNaN(flags.wrapLength) ? 40 : flags.wrapLength,
        mode: flags.mode
    });

    send(`\`\`\`${escapeCodeBlock(theCowSays)}\`\`\``, message);
}, {
    flags: {
        alias: {
            e: 'eyes',
            t: 'tongue',
            w: 'wrap',
            m: 'mode'
        },
        coerce: {
            wrap: boolean       
        },
        number: ['wrapLength']
    }
});