import eightBall from '@resynth1943/eight-ball';
import { command } from 'utilities/commands';
import { send, escapeCodeBlock } from 'utilities/messages';

export default command(async (message, inspected) => {
    const { input } = inspected;

    if (input.length === 0) {
        await send(`You have to ask me a question!`, message);
        return;
    }

    const response = eightBall();
    await send(`\`\`\`${escapeCodeBlock(response)}\`\`\``, message);
}, {
    usage: 'eightball',
    description: 'Ask the mythical Eight Ball a question.'
});
