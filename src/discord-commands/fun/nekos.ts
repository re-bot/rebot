/** @prettier */

import { command } from 'utilities/commands';
import { getRandomNekoImageURL } from 'utilities/images/fun-images';

export default command(
	(message, inspected) => {
		message.channel.createMessage({
			embed: {
				title: "Here's your neko!",
				image: {
					url: getRandomNekoImageURL()
				}
			}
		});
	},
	{
		usage: 'neko',
		description: 'Returns a random Neko image fetched from the http://electrohaxz.tk Neko image API.',
		nsfw: true
	}
);
