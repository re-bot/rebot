/** @prettier */

import { command } from 'utilities/commands';
import { getRandomHeadpatImageURL } from 'utilities/images/fun-images';
import { ResolvableColorName } from 'all-types';

export default command(
	(message, inspected) => {
		message.channel.createMessage({
			embed: {
				image: {
					url: getRandomHeadpatImageURL()
				},
				color: ResolvableColorName.DarkBlue
			}
		});
	},
	{
		usage: 'headpat',
		description: 'Returns a random Headpat image fetched from the http://electrohaxz.tk Headpat image API.'
	}
);
