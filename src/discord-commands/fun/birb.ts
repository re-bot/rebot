/** @prettier */

import { command } from 'utilities/commands';
import { getRandomBirbImageURL } from 'utilities/images/fun-images';

export default command(
	(message, inspected) => {
		message.channel.createMessage({
			embed: {
				image: {
					url: getRandomBirbImageURL()
				}
			}
		});
	},
	{
		usage: 'birb',
		description: 'Returns a random Birb image fetched from the http://electrohaxz.tk Birb image API.'
	}
);
