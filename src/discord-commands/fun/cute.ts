/** @prettier */

import { command } from 'utilities/commands';
import { getRandomCuteImageURL } from 'utilities/images/fun-images';

export default command(
	(message, inspected) => {
		message.channel.createMessage({
			embed: {
				title: 'Cute!',
				image: {
					url: getRandomCuteImageURL()
				}
			}
		});
	},
	{
		usage: 'cute',
		description: 'Returns a random cute image fetched from the http://electrohaxz.tk cute image API.'
	}
);
