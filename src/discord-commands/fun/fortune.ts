import { command } from "utilities/commands";
import fortuneCookies from 'fortune-cookie';
import { randomArrayItem } from "utilities/general-util";
import { escapeCodeBlock } from "utilities/messages";

export default command((message, inspected) => {
    const currentFortune = escapeCodeBlock(randomArrayItem(fortuneCookies));
    message.channel.createMessage(`\`\`\`${currentFortune}\`\`\``);
});