import { command } from "utilities/commands";
import fetch from 'got';
import { send, escapeCodeBlock } from "utilities/messages";
import { EmbedOptions } from "eris";
import { logError } from "utilities/logging";
import { manSectionRe } from "r-constants";
import { ResolvableColorName } from "all-types";

export default command (async (message, inspected) => {
    const { input } = inspected;

    if (manSectionRe.test(input)) {
        // The user is referring to a specific item. Let's get that for them.
        const [, name, article] = manSectionRe.exec(input)!;

        try {
            const { body: response } = await fetch(`https://mankier.com/api/v2/mans/${name}.${article}`, {
                json: true
            });

            return send({
                embed: {
                    title: `\`man\` page for \`${name}(${article})\``,
                    fields: response.sections.map((section: Record<string, any>) => {
                        return {
                            name: section.title,
                            value: section.url
                        };
                    }),
                    color: ResolvableColorName.Blue
                }
            }, message);
        } catch (httpError) {
            return send({
                embed: {
                    title: `Could not find ${name}(${article})`,
                    description: 'The requested \`man\` page could not be found. Perhaps you should try another name, or correct any typos.',
                    color: ResolvableColorName.Red
                }
            }, message);
        }
    }

    try {
        const { body: json } = await fetch(`https://mankier.com/api/v2/mans/?q=${input}`, {
            json: true
        });

        await send({
            embed: {
                title: `\`man\` pages for \`${escapeCodeBlock(input)}\``,
                fields: json.results.map((item: Record<string, any>) => {
                    return {
                        inline: true,
                        name: `\`${item.name}\`${item.section ? `(${item.section})` : ''} ${item.is_alias ? `(alias of [this](${item.url}))` : ''}`,
                        value: item.description
                    };
                }),
                color: ResolvableColorName.Blue
            }
        }, message);
    } catch (error) {
            await send({
                embed: {
                    title: 'Sorry. I couldn\'t do that.',
                    description: 'Something went wrong here. An error, if you will. Resynth is on the case!',
                    color: ResolvableColorName.Red
                }
            }, message);
            logError(error);
    }
}, {
    usage: 'man command[(section}]',
    description: 'Gets the \`man\` page for a specific command. If a section is also passed, e.g "ls(1)", then details for that specific command are returned.'
});

export const explain = command (async (message, inspected, flags) => {
    // FIXME: hack until subcommands get nice treatment
    const input = inspected.input.split(' ').slice(1).join(' ');

    try {
        const { body: explainer } = await fetch(`https://mankier.com/api/v2/explain/?cols=10&q=${input}`);

        send({
            embed: {
                title: `What the \`${input}\` shell command means`,
                description: `\`\`\`${escapeCodeBlock(explainer)}\`\`\``,
                color: ResolvableColorName.Blue
            }
        }, message);
    } catch (httpError) {
        console.log(JSON.stringify(httpError));
        if (httpError.statusCode === 404) {
            return send({
                embed: {
                    title: `Could not explain that command.`,
                    description: 'The requested explainer page could not be found. Perhaps you should try another name, or correct any typos.',
                    color: ResolvableColorName.Red
                }
            }, message);
        } else {
            await send({
                embed: {
                    title: 'Sorry. I couldn\'t do that.',
                    description: 'Something went wrong here. An error, if you will. Resynth is on the case!',
                    color: ResolvableColorName.Red
                }
            }, message);

            logError(httpError);
        }
    }
}, {
    usage: 'explain shellcommand',
    description: 'Explains what a shell command does. Uses the https://www.mankier.com/ API.',
    flags: {

    }
});
