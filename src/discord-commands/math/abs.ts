import { createMathCommand } from "utilities/discord";

export default createMathCommand(Math.abs, 1, {
	description: 'Get the absolute value of a number.',
	usage: 'abs 0.5'
});