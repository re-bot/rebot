import { createMathCommand } from "utilities/discord";

export default createMathCommand(Math.atan, 1, {
	description: 'Get the arctangent (in radians) of a number.',
	usage: 'atan 8'
});