import { createMathCommand } from "utilities/discord";

export default createMathCommand(Math.ceil, 1, {
	description: 'Round a number up to the next largest whole number or integer.',
	usage: 'ceil 7.004'
});