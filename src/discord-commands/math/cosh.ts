import { createMathCommand } from "utilities/discord";

export default createMathCommand(Math.cos, 1, {
	description: 'Get the hyperbolic cosine of a number, that can be expressed using the constant e.',
	usage: 'cosh -1'
});