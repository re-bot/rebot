import { createMathCommand } from "utilities/discord";

export default createMathCommand(Math.log10, 1, {
	description: 'Get the base 10 logarithm of a number.',
	usage: 'log10 2'
});