import { createMathCommand } from "utilities/discord";

export default createMathCommand(Math.tanh, 1, {
	description: 'Gets the hyperbolic tangent of a number.',
	usage: 'tanh -2'
});
