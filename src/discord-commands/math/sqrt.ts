import { createMathCommand } from "utilities/discord";

export default createMathCommand(Math.sqrt, 1, {
	description: 'Gets the square root of a number.',
	usage: 'sqrt 3'
});
