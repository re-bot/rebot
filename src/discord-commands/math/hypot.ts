import { createMathCommand } from "utilities/discord";

export default createMathCommand(Math.hypot, Infinity, {
	description: 'Get the square root of the sum of squares of the arguments.',
	usage: 'hypot 3 4 5'
});