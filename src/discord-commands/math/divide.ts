import { createBasicMathCommand } from "utilities/discord";

export default createBasicMathCommand((...args) => args.reduce((x, y) => x / parseFloat(y), 0), {
    description: 'Divides all the arguments.',
    usage: 'divide 2 4 3'
});