import { createMathCommand } from "utilities/discord";

export default createMathCommand(Math.acos, 1, {
	description: 'Get the arccosine (in radians) of a number.',
	usage: 'acos 5'
});