import { createBasicMathCommand } from "utilities/discord";

export default createBasicMathCommand((...args) => args.reduce((x, y) => x + parseFloat(y), 0), {
    description: 'Get the value of all the arguments timesd together.',
    usage: 'times 2 4'
});