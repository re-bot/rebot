import { createMathCommand } from "utilities/discord";

export default createMathCommand(Math.atanh, 1, {
	description: 'Get the hyperbolic arctangent of a number.',
	usage: 'atanh -1'
});