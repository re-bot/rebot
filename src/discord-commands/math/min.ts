import { createMathCommand } from "utilities/discord";

export default createMathCommand(Math.min, Infinity, {
	description: 'Get the smallest of zero or more numbers.',
	usage: 'min 0 0.1 -2'
});