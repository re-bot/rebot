import { createMathCommand } from "utilities/discord";

export default createMathCommand(Math.log, 1, {
	description: 'Get the natural logarithm (base `e`) of a number.',
	usage: 'log 1'
});