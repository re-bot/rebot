import { createMathCommand } from "utilities/discord";

export default createMathCommand(Math.sinh, 1, {
	description: 'Gets the hyperbolic sine of a number, that can be expressed using the constant `e`.',
	usage: 'sinh -1'
});
