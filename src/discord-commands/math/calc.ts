import { evaluate } from 'mathjs';
import { command } from 'utilities/commands';
import { send } from 'utilities/messages';

export default command((message, inspected) => {
    const scope: Record<string, string> = {};
    const result = evaluate(inspected.input, scope);
    const variablesToString = Object.keys(scope).reduce((strings, key) => {
        strings.push(`${key} = ${scope[key]}`);
        return strings;
    }, [] as string[]).join(', ');

    send(`\`\`\`${result}\n${variablesToString.length > 0 ? `\`\`\`${variablesToString}\`\`\`` : ''}`, message);
});
