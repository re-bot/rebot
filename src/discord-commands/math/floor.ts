import { createMathCommand } from "utilities/discord";

export default createMathCommand(Math.floor, 1, {
	description: 'Get the largest integer less than or equal to a given number.',
	usage: 'floor 2.5'
});