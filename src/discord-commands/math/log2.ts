import { createMathCommand } from "utilities/discord";

export default createMathCommand(Math.log2, 1, {
	description: 'Get the base 2 logarithm of a number.',
	usage: 'log2 3'
});