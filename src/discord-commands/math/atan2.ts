import { createMathCommand } from "utilities/discord";

export default createMathCommand(Math.atan2, 2, {
	description: 'Get the angle in the plane (in radians) between the positive x-axis and the ray from (0,0) to the point (x,y).', 
	usage: 'atan2 5 10'
});