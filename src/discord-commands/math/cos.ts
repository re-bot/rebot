import { createMathCommand } from "utilities/discord";

export default createMathCommand(Math.cos, 1, {
	description: 'Get the cosine of the specified angle, which must be specified in radians.',
	usage: 'cos 2'
});