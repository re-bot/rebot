import { createMathCommand } from "utilities/discord";

export default createMathCommand(Math.cbrt, 1, {
	description: 'Get the cube root of a number.',
	usage: 'cbrt 64'
});