import { createBasicMathCommand } from "utilities/discord";

export default createBasicMathCommand((...args) => args.reduce((x, y) => x + parseFloat(y), 0), {
    description: 'Adds all the arguments together.',
    usage: 'add 1 2 3'
});