import { createMathCommand } from "utilities/discord";

export default createMathCommand(Math.sign, 1, {
	description: 'Gets either a positive or negative `+/- 1`, indicating the sign of a number passed into the argument. If the number passed into this command is 0, it will return a `+/- 0`. Note that if the number is positive, an explicit (+) will not be returned.',
	usage: 'sign -3'
});
