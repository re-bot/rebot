import { createMathCommand } from "utilities/discord";

export default createMathCommand(Math.sin, 1, {
	description: 'Gets the sine of a number.',
	usage: 'sin 1'
});
