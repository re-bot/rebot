import { createMathCommand } from "utilities/discord";

export default createMathCommand(Math.expm1, 1, {
	description: 'Get `ex - 1`, where `x` is the argument, and `e` the base of the natural logarithms.',
	usage: 'expm1 1'
});