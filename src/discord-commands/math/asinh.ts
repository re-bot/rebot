import { createMathCommand } from "utilities/discord";

export default createMathCommand(Math.asinh, 1, {
	description: 'Get the hyperbolic arcsine of a number.',
	usage: 'asinh 2'
});
