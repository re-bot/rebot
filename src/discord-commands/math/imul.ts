import { createMathCommand } from "utilities/discord";

export default createMathCommand(Math.imul, 2, {
	description: 'Get the result of the C-like 32-bit multiplication of the two parameters.',
	usage: 'imul -5 12'
});