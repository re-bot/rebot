import { createMathCommand } from "utilities/discord";

export default createMathCommand(Math.clz32, 1, {
	description: 'Get the number of leading zero bits in the 32-bit binary representation of a number.',
	usage: 'clz32 4'
});