import { createMathCommand } from "utilities/discord";

export default createMathCommand(Math.round, 1, {
	description: 'Get the value of a number rounded to the nearest integer.',
	usage: 'round 2.5'
});
