import { createMathCommand } from "utilities/discord";

export default createMathCommand(Math.acosh, 1, {
	description: 'Get the hyperbolic arc-cosine of a number.',
	usage: 'acosh 2'
});