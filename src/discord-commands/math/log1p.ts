import { createMathCommand } from "utilities/discord";

export default createMathCommand(Math.log1p, 1, {
	description: 'Get the natural logarithm (base `e`) of `1 + a number`.',
	usage: 'log1p 1'
});