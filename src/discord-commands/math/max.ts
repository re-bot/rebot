import { createMathCommand } from "utilities/discord";

export default createMathCommand(Math.max, Infinity, {
	description: 'Get the largest of zero or more numbers.',
	usage: 'max 1 2 3 7 9'
});