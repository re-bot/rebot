import { createMathCommand } from "utilities/discord";

export default createMathCommand(Math.asin, 1, {
	description: 'Get the arcsine (in radians) of a number.',
	usage: 'asin 6'
});