import { createMathCommand } from "utilities/discord";

export default createMathCommand(Math.trunc, 1, {
	description: 'Gets the integer part of a number by removing any fractional digits.',
	usage: 'trunc 13.37'
});
