import { createMathCommand } from "utilities/discord";

export default createMathCommand(Math.fround, 1, {
	description: 'Get the nearest 32-bit single precision float representation of a JavaScript `Number`.',
	usage: 'fround 5.05'
});