import { createMathCommand } from "utilities/discord";

export default createMathCommand(Math.exp, 1, {
	description: 'Get `ex`, where `x` is the argument, and `e` is Euler\'s number (also known as Napier\'s constant), the base of the natural logarithms.',
	usage: 'exp -1'
});