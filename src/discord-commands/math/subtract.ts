import { createBasicMathCommand } from "utilities/discord";

export default createBasicMathCommand((...args) => args.reduce((x, y) => x - parseFloat(y), 0));