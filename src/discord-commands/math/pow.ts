import { createMathCommand } from "utilities/discord";

export default createMathCommand(Math.pow, 2, {
	description: 'Get the base to the exponent power, that is, `baseᵉˣᵖᵒⁿᵉⁿᵗ`.',
	usage: 'pow 7 3'
});
