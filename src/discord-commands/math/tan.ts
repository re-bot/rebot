import { createMathCommand } from "utilities/discord";

export default createMathCommand(Math.tan, 1, {
	description: 'Gets the tangent of a number.',
	usage: 'tan 3'
});
