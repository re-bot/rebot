import { resolve } from 'path';
import { UselessGuildCheck, ResolvableColorName } from "all-types";
import { EmbedOptions } from "eris";
import { OperationResult } from 'all-types';
import { env } from 'utilities/general-util';

/** The path to the services directory. */
export const servicesPath = resolve(__dirname, './services/');

/** Matches a channel mention, e.g `<#id>`. */
export const reChannelMention = /^<#(\d+)>$/;

/** Matches an ID of any kind. You should always check it's a valid ID after this, though. */
export const reID = /^\d{5,}$/;

/** Matches a user mention, e.g `<@id>`. */
export const reUserMention = /^<@(\d+)>$/;

/** Matches `yes`, `y`, `true`, 1, `n`, `no`, `false` and 0. Useful for checking if user input can be converted to a
 * boolean by the `yn` module. */
export const reYesNo = /^(?:y|yes|true|1|n|no|false|0)$/i;


/* eslint-disable no-process-env */
/** The main token Rebot should use to login to Discord. This depends on the NODE_ENV variable. */
export const token = env({
	production: process.env.DISCORD_TOKEN,
	development: process.env.DISCORD_DEV_TOKEN
})!;

export const prefix = env({
	production: process.env.DISCORD_PREFIX,
	development: process.env.DISCORD_DEV_PREFIX
})!;

/** The ID of Rebot's testing / home server. */
export const rebotServerID = process.env.DISCORD_SERVER_ID;

/* Honestly, the ! assertions I would need if I didn't use "!" here isn't worth it. It's a bit of a hack, yes, and it's dangerous as it marks a volatile variable as safe, but Rebot should catch this
 * before everything boots up, so it shouldn't be a problem. */
export const owners = process.env.DISCORD_OWNERS?.split(',')!;

export const nodeEnv: string = process.env.NODE_ENV as string;
/* eslint-enable no-process-env */

/** A list of the environmental variables required for Rebot to run correctly. */
export const requiredEnvironmentalVariables = ['DISCORD_DEV_TOKEN', 'DISCORD_TOKEN', 'REBOT_SOCKET_PATH', 'XDG_RUNTIME_DIR', 'DISCORD_DEV_PREFIX', 'DISCORD_PREFIX', 'NODE_ENV', 'DISCORD_SERVER_ID', 'LOGROTATE_SIZE', 'LOGROTATE_INTERVAL'];

/** Equates to `:white_check_mark:`. */
export const successEmoji = '✅';

/** Equates to `:ballot_box_with_check:`. */
export const successEmojiAlt = '☑';

/** Equates to `:x:`. */
export const failEmoji = '🇽';

/** Equates to `:regional_indicator_x:`. */
export const failEmojiAlt = '🇽';

/** Equates to `:timer:`. */
export const timerEmoji = '⏲';

/** Equates to `:alarm_clock:`. */
export const timerEmojiAlt = '⏰';

/** Equates to `:lock:`. */
export const accessDeniedEmoji = '🔒';

/** Equates to `:unlock:`. */
export const accessGrantedEmoji = '🔓';

/** A mapping of `OperationResult`s to their respective emojis. */
export const OperationResultEmojis: Record<OperationResult, string> = {
	[OperationResult.Failed]: failEmoji,
	[OperationResult.Successful]: successEmoji,
	[OperationResult.AccessDenied]: accessDeniedEmoji,
	[OperationResult.AccessGranted]: accessGrantedEmoji
};

/** Equates to `:arrow_backward:`. */
export const backwardEmoji = '◀';

/** Equates to `:arrow_forward:`. */
export const forwardEmoji = '▶';

/** Equates to `:1234:`. */
export const numbersEmoji = '🔢';

/** Equates to `:previous_track:`. */
export const previousEmoji = '⏮';

/** Equates to `:track_next:`. */
export const nextEmoji = '⏭';

/** Equates to `:robot:`. */
export const robotEmoji = '🤖';

/** Equates to `:x:`. */
export const cancelEmoji = '❌';

export const wordsToNumbers: Record<string, number> = {
	one: 1,
	two: 2,
	three: 3,
	four: 4,
	five: 5,
	six: 6,
	seven: 7,
	eight: 8,
	nine: 9,
	ten: 10,
	eleven: 11,
	twelve: 12,
	thirteen: 13,
	fourteen: 14,
	fifteen: 15,
	sixteen: 16,
	seventeen: 17,
	eighteen: 18,
	nineteen: 19,
	twenty: 20,
	twentyone: 21,
	twentytwo: 22,
	twentythree: 23,
	twentyfour: 24,
	twentyfive: 25,
	twentysix: 26,
	twentyseven: 27,
	twentyeight: 28,
	twentynine: 29,
	thirty: 30
};

export const numbersToWords: Record<number, string> = {
	1: 'one',
	2: 'two',
	3: 'three',
	4: 'four',
	5: 'five',
	6: 'six',
	7: 'seven',
	8: 'eight',
	9: 'nine',
	10: 'ten',
	11: 'eleven',
	12: 'twelve',
	13: 'thirteen',
	14: 'fourteen',
	15: 'fifteen',
	16: 'sixteen',
	17: 'seventeen',
	18: 'eighteen',
	19: 'nineteen',
	20: 'twenty',
	21: 'twentyone',
	22: 'twentytwo',
	23: 'twentythree',
	24: 'twentyfour',
	25: 'twentyfive',
	26: 'twentysix',
	27: 'twentyseven',
	28: 'twentyeight',
	29: 'twentynine',
	30: 'thirty'
};

/** The path to the commands directory. */
export const commandsPath = resolve(__dirname, './discord-commands/');

/** A unique marker for a Command. */
export const commandMarker = Symbol('rebot.command');

/** A map of channel names to their type: https://discordapp.com/developers/docs/resources/channel#channel-object-channel-types */
export enum ChannelType {
	GuildText = 0,
	DirectMessage = 1,
	GuildVoice = 2,
	GroupDirectMessage = 3,
	GuildCategory = 4,
	GuildNews = 5,
	GuildStore = 6
};

/** The minimum amount of members expected for a guild not to be classed as "useless". */
export const minimumMembers = 3;

/** A map of `UselessGuildCheck` reasons to their friendly messages. */
export const uselessGuildCheckMessages: Record<keyof UselessGuildCheck, string> = {
	hasAnyAvailableChannels: 'There aren\'t any available text channels in your server. Try creating one!',
	hasMinimumMembers: `You don\'t have enough members in your server for Rebot to be of any good (the minimum is ${minimumMembers}). Try inviting people!`
}

/** The ID of the Rebot Suggestions webhook. */
export const suggestionsWebhookID = '634698394551582730';

/** The token of the Rebot Suggestions webhook. */
export const suggestionsWebhookToken = 'FWg2PDX0jVQLPfZ82HOmY_RpLJQB_Hn7XoywBALc-3F2eU8W5SQ-ahNVLTI3ryNp9vqV';

/** The embed sent from the `about` command. Exported to a member for reusability. */
export const aboutEmbed: EmbedOptions = {
	title: 'Hello. I\'m Rebot.',
	description: `I'm a Discord bot ${robotEmoji}. I can search the web, show you ~~cool~~ images, moderate your server, and other fun stuff.`,
	color: ResolvableColorName.Green,
	footer: {
		text: `Try ${prefix}help to see a list of commands.`
	}
}

/** Every single Figlet font. */
export const figletFonts = [
	'1Row',
	'3-D',
	'3D Diagonal',
	'3D-ASCII',
	'3x5',
	'4Max',
	'5 Line Oblique',
	'AMC 3 Line',
	'AMC 3 Liv1',
	'AMC AAA01',
	'AMC Neko',
	'AMC Razor',
	'AMC Razor2',
	'AMC Slash',
	'AMC Slider',
	'AMC Thin',
	'AMC Tubes',
	'AMC Untitled',
	'ANSI Shadow',
	'ASCII New Roman',
	'Acrobatic',
	'Alligator',
	'Alligator2',
	'Alpha',
	'Alphabet',
	'Arrows',
	'Avatar',
	'B1FF',
	'Banner',
	'Banner3-D',
	'Banner3',
	'Banner4',
	'Barbwire',
	'Basic',
	'Bear',
	'Bell',
	'Benjamin',
	'Big Chief',
	'Big Money-ne',
	'Big Money-nw',
	'Big Money-se',
	'Big Money-sw',
	'Big',
	'Bigfig',
	'Binary',
	'Block',
	'Blocks',
	'Bloody',
	'Bolger',
	'Braced',
	'Bright',
	'Broadway KB',
	'Broadway',
	'Bubble',
	'Bulbhead',
	'Caligraphy',
	'Caligraphy2',
	'Calvin S',
	'Cards',
	'Catwalk',
	'Chiseled',
	'Chunky',
	'Coinstak',
	'Cola',
	'Colossal',
	'Computer',
	'Contessa',
	'Contrast',
	'Cosmike',
	'Crawford',
	'Crawford2',
	'Crazy',
	'Cricket',
	'Cursive',
	'Cyberlarge',
	'Cybermedium',
	'Cybersmall',
	'Cygnet',
	'DANC4',
	'DOS Rebel',
	'DWhistled',
	'Dancing Font',
	'Decimal',
	'Def Leppard',
	'Delta Corps Priest 1',
	'Diamond',
	'Diet Cola',
	'Digital',
	'Doh',
	'Doom',
	'Dot Matrix',
	'Double Shorts',
	'Double',
	'Dr Pepper',
	'Efti Chess',
	'Efti Font',
	'Efti Italic',
	'Efti Piti',
	'Efti Robot',
	'Efti Wall',
	'Efti Water',
	'Electronic',
	'Elite',
	'Epic',
	'Fender',
	'Filter',
	'Fire Font-k',
	'Fire Font-s',
	'Flipped',
	'Flower Power',
	'Four Tops',
	'Fraktur',
	'Fun Face',
	'Fun Faces',
	'Fuzzy',
	'Georgi16',
	'Georgia11',
	'Ghost',
	'Ghoulish',
	'Glenyn',
	'Goofy',
	'Gothic',
	'Graceful',
	'Gradient',
	'Graffiti',
	'Greek',
	'Heart Left',
	'Heart Right',
	'Henry 3D',
	'Hex',
	'Hieroglyphs',
	'Hollywood',
	'Horizontal Left',
	'Horizontal Right',
	'ICL-1900',
	'Impossible',
	'Invita',
	'Isometric1',
	'Isometric2',
	'Isometric3',
	'Isometric4',
	'Italic',
	'Ivrit',
	'JS Block Letters',
	'JS Bracket Letters',
	'JS Capital Curves',
	'JS Cursive',
	'JS Stick Letters',
	'Jacky',
	'Jazmine',
	'Jerusalem',
	'Katakana',
	'Kban',
	'Keyboard',
	'Knob',
	'Konto Slant',
	'Konto',
	'LCD',
	'Larry 3D 2',
	'Larry 3D',
	'Lean',
	'Letters',
	'Lil Devil',
	'Line Blocks',
	'Linux',
	'Lockergnome',
	'Madrid',
	'Marquee',
	'Maxfour',
	'Merlin1',
	'Merlin2',
	'Mike',
	'Mini',
	'Mirror',
	'Mnemonic',
	'Modular',
	'Morse',
	'Morse2',
	'Moscow',
	'Mshebrew210',
	'Muzzle',
	'NScript',
	'NT Greek',
	'NV Script',
	'Nancyj-Fancy',
	'Nancyj-Improved',
	'Nancyj-Underlined',
	'Nancyj',
	'Nipples',
	'O8',
	'OS2',
	'Octal',
	'Ogre',
	'Old Banner',
	"Patorjk's Cheese",
	'Patorjk-HeX',
	'Pawp',
	'Peaks Slant',
	'Peaks',
	'Pebbles',
	'Pepper',
	'Poison',
	'Puffy',
	'Puzzle',
	'Pyramid',
	'Rammstein',
	'Rectangles',
	'Red Phoenix',
	'Relief',
	'Relief2',
	'Reverse',
	'Roman',
	'Rot13',
	'Rotated',
	'Rounded',
	'Rowan Cap',
	'Rozzo',
	'Runic',
	'Runyc',
	'S Blood',
	'SL Script',
	'Santa Clara',
	'Script',
	'Serifcap',
	'Shadow',
	'Shimrod',
	'Short',
	'Slant Relief',
	'Slant',
	'Slide',
	'Small Caps',
	'Small Isometric1',
	'Small Keyboard',
	'Small Poison',
	'Small Script',
	'Small Shadow',
	'Small Slant',
	'Small Tengwar',
	'Small',
	'Soft',
	'Speed',
	'Spliff',
	'Stacey',
	'Stampate',
	'Stampatello',
	'Standard',
	'Star Strips',
	'Star Wars',
	'Stellar',
	'Stforek',
	'Stick Letters',
	'Stop',
	'Straight',
	'Stronger Than All',
	'Sub-Zero',
	'Swamp Land',
	'Swan',
	'Sweet',
	'THIS',
	'Tanja',
	'Tengwar',
	'Term',
	'Test1',
	'The Edge',
	'Thick',
	'Thin',
	'Thorned',
	'Three Point',
	'Ticks Slant',
	'Ticks',
	'Tiles',
	'Tinker-Toy',
	'Tombstone',
	'Train',
	'Trek',
	'Tsalagi',
	'Tubular',
	'Twisted',
	'Two Point',
	'USA Flag',
	'Univers',
	'Varsity',
	'Wavy',
	'Weird',
	'Wet Letter',
	'Whimsy',
	'Wow'
];

export const guildSettingsPrettyNames: Record<string, string> = {
	muteChannelID: 'Mute Channel ID',
	disabledCommands: 'Disabled Commands'
}

export interface GuildSettingsMap {
	muteChannelID: string;
	disabledCommands: string[];
}

export const unixSocketPath = process.env.REBOT_RUNTIME_DIR ??
	process.env.XDG_RUNTIME_DIR ??
	process.env.UID ?? `/run/user/${process.env.UID}/rebot/`;

export const logFilePath = resolve(__dirname, '../../logs/');

export const manSectionRe = /^(\w+)\((\d+)\)$/;
export const ansiEscapeCodeRe = /[\u001b\u009b][[()#;?]*(?:[0-9]{1,4}(?:;[0-9]{0,4})*)?[0-9A-ORZcf-nqry=><]/g;