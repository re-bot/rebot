/** @prettier */

import { Message, TextChannel, EmbedOptions, GuildChannel, Emoji } from 'eris';
import { Source } from 'utilities/source';
import { FlagCoercionError } from 'utilities/yargs-coercion';

/** A standard functional Discord command. */
export type BaseDiscordCommand = (
	message: Message,
	inspected: InspectedArguments,
	flags: YargsParser.Arguments
) => void;

/** Represents the activation state of a command. */
export enum CommandState {
	Disabled,
	Enabled,
	UnderMaintenance
}

/** Every Discord permission. */
export type PermissionNode = 
	| 'CREATE_INSTANT_INVITE'
	| 'KICK_MEMBERS'
	| 'BAN_MEMBERS'
	| 'ADMINISTRATOR'
	| 'MANAGE_CHANNELS'
	| 'MANAGE_GUILD'
	| 'ADD_REACTIONS'
	| 'VIEW_AUDIT_LOG'
	| 'VIEW_CHANNEL'
	| 'SEND_MESSAGES'
	| 'SEND_TTS_MESSAGES'
	| 'MANAGE_MESSAGES'
	| 'EMBED_LINKS'
	| 'ATTACH_FILES'
	| 'READ_MESSAGE_HISTORY'
	| 'MENTION_EVERYONE'
	| 'USE_EXTERNAL_EMOJIS'
	| 'CONNECT'
	| 'SPEAK'
	| 'MUTE_MEMBERS'
	| 'USE_VAD'
	| 'PRIORITY_SPEAKER'
	| 'STREAM'
	| 'CHANGE_NICKNAME'
	| 'MANAGE_NICKNAMES'
	| 'MANAGE_ROLES'
	| 'MANAGE_WEBHOOKS'
	| 'MANAGE_EMOJIS'

/** Represents the permissions required for a user to run a command. */
export interface Permissions {
	roles?: string[];
	nodes?: PermissionNode[];
	isOwnerCommand?: boolean;
}

/** Options passed to the `command` function go here. */
export interface CommandOptions {
	permissions?: Permissions;
	usage?: string;
	flags?: YargsParser.Options;
	sources?: Source[];
	description?: string;
	nsfw?: boolean;
	subCommands?: Record<string, DiscordCommand>;
	state?: CommandState;
}

export interface InspectedArguments {
	base: string;
	split: string[];
	command: string;
	args: string[];
	input: string;
	flags: Record<string, unknown>;
}

export type DiscordCommand = BaseDiscordCommand &
	CommandOptions & { path: string; category: string; modName: string; subCommands: Record<string, DiscordCommand> };

export type Predicate<Value> = (value: Value) => boolean;

/** This is passed to `send` to describe the state of a message. */
export enum OperationResult {
	/** The requested operation has failed. */
	Failed,

	/** The request operation has successfully completed. */
	Successful,

	/** The user does not have permission to execute this action. */
	AccessDenied,

	/** The user *does* have permission to excecute this action. */
	AccessGranted
}

/** Describes an extended yargs corecion function that hasn't been passed through `typedCoercions`. */
export type FlagCoercionFunction<SuccessfulReturnValue = unknown> = (
	value: unknown
) => FlagCoercionError | SuccessfulReturnValue;

// /** Describes a yargs coercion function that *has* been adapted for yargs (`FlagCoercionFunction` describes a function that hasn't been passed through `typedCoercions`). */
// export type YargsCoercionFunction<SuccessfulReturnValue = unknown> = (value: unknown, flagName: string) => FlagCoercionError | SuccessfulReturnValue;

export type SendTarget = Message | TextChannel;

/* eslint-disable no-magic-numbers */
/** A map of names to their appropriate hex values. */
export enum ResolvableColorName {
	Default = 0x000000,
	White = 0xffffff,
	Aqua = 0x1abc9c,
	Green = 0x2ecc71,
	Blue = 0x3498db,
	Yellow = 0xffff00,
	Purple = 0x9b59b6,
	LuminousVividPink = 0xe91e63,
	Gold = 0xf1c40f,
	Orange = 0xe67e22,
	Red = 0xe74c3c,
	Grey = 0x95a5a6,
	DarkerGrey = 0x7f8c8d,
	Navy = 0x34495e,
	DarkAqua = 0x11806a,
	DarkGreen = 0x1f8b4c,
	DarkBlue = 0x206694,
	DarkPurple = 0x71368a,
	DarkVividPink = 0xad1457,
	DarkGold = 0xc27c0e,
	DarkOrange = 0xa84300,
	DarkRed = 0x992d22,
	DarkGrey = 0x979c9f,
	LightGrey = 0xbcc0c0,
	DarkNavy = 0x2c3e50,
	Random,
	Blurple = 0x7289da,
	Greyple = 0x99aab5,
	DarkButNotBlack = 0x2c2f33,
	NotQuiteBlack = 0x23272a
}
/* eslint-enable no-magic-numbers */

/** Represents a color that can be resolved into a numerical value via the `resolveColor` utility. */
export type ResolvableColor =
	// Represents an RGB array.
	[number, number, number] | ResolvableColorName;

/** Represents the options object passed to the `escapeMarkdown` utility. */
export interface EscapeMarkdownOptions {
	codeBlock?: boolean;
	inlineCode?: boolean;
	bold?: boolean;
	italic?: boolean;
	underline?: boolean;
	strikethrough?: boolean;
	spoiler?: boolean;
	codeBlockContent?: boolean;
	inlineCodeContent?: boolean;
}

export interface CleanContentOptions {
	everyoneOrHere?: boolean;
	userMentions?: boolean;
	channelMentions?: boolean;
	roleMentions?: boolean;
}

export namespace YargsParser {
	export interface Arguments {
		/** Non-option arguments */
		_: string[];
		/** The script name or node command */
		$0: string;
		/** All remaining options */
		[argName: string]: any;
	}

	export interface DetailedArguments {
		/** An object representing the parsed value of `args` */
		argv: Arguments;
		/** Populated with an error object if an exception occurred during parsing. */
		error: Error | null;
		/** The inferred list of aliases built by combining lists in opts.alias. */
		aliases: { [alias: string]: string[] };
		/** Any new aliases added via camel-case expansion. */
		newAliases: { [alias: string]: boolean };
		/** The configuration loaded from the yargs stanza in package.json. */
		configuration: Configuration;
	}

	export interface Configuration {
		/** Should variables prefixed with --no be treated as negations? Default is `true` */
		'boolean-negation': boolean;
		/** Should hyphenated arguments be expanded into camel-case aliases? Default is `true` */
		'camel-case-expansion': boolean;
		/** Should arrays be combined when provided by both command line arguments and a configuration file. Default
		 * is `false` */
		'combine-arrays': boolean;
		/** Should keys that contain . be treated as objects? Default is `true` */
		'dot-notation': boolean;
		/** Should arguments be coerced into an array when duplicated. Default is `true` */
		'duplicate-arguments-array': boolean;
		/** Should array arguments be coerced into a single array when duplicated. Default is `true` */
		'flatten-duplicate-arrays': boolean;
		/** Should parsing stop at the first text argument? This is similar to how e.g. ssh parses its command line.
		 * Default is `false` */
		'halt-at-non-option': boolean;
		/** The prefix to use for negated boolean variables. Default is `'no-'` */
		'negation-prefix': string;
		/** Should keys that look like numbers be treated as such? Default is `true` */
		'parse-numbers': boolean;
		/** Should unparsed flags be stored in -- or _. Default is `false` */
		'populate--': boolean;
		/** Should a placeholder be added for keys not set via the corresponding CLI argument? Default is `false` */
		'set-placeholder-key': boolean;
		/** Should a group of short-options be treated as boolean flags? Default is `true` */
		'short-option-groups': boolean;
	}

	export interface Options {
		/** An object representing the set of aliases for a key: `{ alias: { foo: ['f']} }`. */
		alias?: { [key: string]: string | string[] };
		/**
		 * Indicate that keys should be parsed as an array: `{ array: ['foo', 'bar'] }`.
		 * Indicate that keys should be parsed as an array and coerced to booleans / numbers:
		 * { array: [ { key: 'foo', boolean: true }, {key: 'bar', number: true} ] }`.
		 */
		array?: string[] | Array<{ key: string; boolean?: boolean; number?: boolean }>;
		/** Arguments should be parsed as booleans: `{ boolean: ['x', 'y'] }`. */
		boolean?: string[];
		/** Indicate a key that represents a path to a configuration file (this file will be loaded and parsed). */
		config?: string | string[] | { [key: string]: boolean };
		/** Provide configuration options to the yargs-parser. */
		configuration?: Partial<Configuration>;
		/**
		 * Provide a custom synchronous function that returns a coerced value from the argument provided
		 * (or throws an error), e.g. `{ coerce: { foo: function (arg) { return modifiedArg } } }`.
		 */
		coerce?: { [key: string]: (arg: any) => any };
		/** Indicate a key that should be used as a counter, e.g., `-vvv = {v: 3}`. */
		count?: string[];
		/** Provide default values for keys: `{ default: { x: 33, y: 'hello world!' } }`. */
		default?: { [key: string]: any };
		/** Environment variables (`process.env`) with the prefix provided should be parsed. */
		envPrefix?: string;
		/** Specify that a key requires n arguments: `{ narg: {x: 2} }`. */
		narg?: { [key: string]: number };
		/** `path.normalize()` will be applied to values set to this key. */
		normalize?: string[];
		/** Keys should be treated as strings (even if they resemble a number `-x 33`). */
		string?: string[];
		/** Keys should be treated as numbers. */
		number?: string[];
	}

	export interface Parser {
		(argv: string | string[], opts?: Options): Arguments;
		detailed(argv: string | string[], opts?: Options): DetailedArguments;
	}
}

export interface DotEnvOptions {
	encoding?: string;
	path?: string;
}

/** Represents an item inside an `EmbedCarousel`. */
export interface EmbedCarouselItem extends EmbedOptions {}

/** Options for the `clampString` utility. */
export interface ClampStringOptions {
	ellipsis?: boolean;
}

/** The return type of `isUselessGuild`. */
export interface UselessGuildCheck {
	hasAnyAvailableChannels: boolean;
	hasMinimumMembers: boolean;
}

/** A special permission only users that have Resynth's blessing can use. */
export type RebotPermission = 
	| 'global-tags';

/** How fucked Rebot will be if the service doesn't load in the bootstrapper. */
export enum ServiceSeverityLevel {
	/** If this service doesn't load, Rebot will quit. */
	Critical,

	/** If this service doesn't load, Rebot won't quit. */
	Optional
}

export interface PaginatorDispatchOptions {
	emoji: Emoji;
}
