/** @prettier */

/**
 * This concept is mostly stolen from Blargbot's source code, and I think it
 * could be useful here. This whole thing is a simple concept I'll explain here.
 *
 * After the Watcher (this file) starts the Main (index.ts), the Watcher listens
 * for the "PING" event, which the Main emits. If the "PING" event doesn't get
 * sent from the Main, it is automatically restarted, as it's assumed to be in a
 * crashed state.
 */

import { resolve } from 'path';
import { fork, ChildProcess } from 'child_process';
import Logger from 'js-logger';
import chalk from 'chalk';

/** The amount of PINGs we haven't handled yet. */
let missedTicks = 0;

/** The maximum amount of missed PING events the watcher should wait for. */
const maxMissedTicks = 10;

/** The path to the `index.ts` file. */
const indexPath = resolve(__dirname, './index');

/** The main process. */
let mainProcess: ChildProcess | void;

/** The time to wait before restarting the bot when there's a runtime error. */
let restartBackoff = 10240;

/** Starts the main process. */
function start() {
	Logger.info('Starting the index file under the watcher...');

	mainProcess = fork(indexPath);

	mainProcess.on('message', message => {
		if (message === 'heartbeat') {
			// We've got a new ping from the main process. Now we need to handle it.
			missedTicks = 0;
		} else if (message === 'shutdown') {
			Logger.info('Rebot has requested a shutdown. Shutting down...');
			(mainProcess as ChildProcess).kill();
		} else if (message === 'reboot') {
			Logger.info('Rebot has requested a reboot. Rebooting...');
			(mainProcess as ChildProcess).kill();
			start();
		}
	});

	mainProcess.once('message', () => {
		Logger.info('Started the index file under the watcher. Listening for errors.');
	});

	mainProcess.on('error', error => {
		console.log(`error`, JSON.stringify(error));
	});

	// We can't have it exiting ;(
	mainProcess.on('exit', code => {
		if (code === 0) {
			Logger.info('Rebot exited with a code 0. Goodnight.');
			// If it's a non-error exit, we'll exit too.
			process.exit(0);
		} else {
			setTimeout(start, restartBackoff);
		}
	});
}

start();

setTimeout(() => {
	if (missedTicks > maxMissedTicks) {
		// We need to restart it.
		Logger.info('Restarting Rebot main process due to inactivity (thread locked?).');
		
		if (mainProcess) {
			mainProcess.kill();
		}

		start();
	}

	// Increase the amount of missed ticks.
	++missedTicks;
}, 10000);

Logger.useDefaults({
	formatter(messages) {
		const prefix = `${chalk.red('[WATCHER] ')}:`;
		messages.unshift(prefix);
	}
});
