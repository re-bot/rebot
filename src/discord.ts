/** @prettier */

import { client } from './state';
import Logger from './utilities/logging';
import { getCommand } from 'utilities/commands';
import { testPermissions, checkIfGuildIsUseless } from 'utilities/discord';
import { inspectMessageContent, send } from 'utilities/messages';
import { Message, TextChannel, GuildChannel, Guild } from 'eris';
import { prefix, nodeEnv, ChannelType, aboutEmbed } from 'r-constants';
import yargsParser from 'yargs-parser';
import { getSourceForMessage, getPluralSourceName } from 'utilities/source';
import { FlagCoercionError } from 'utilities/yargs-coercion';
// import { Constants as ErisConstants } from 'eris';

const logger = Logger.get('discord');

/** Connects the Discord client to the API. */
export async function startDiscord() {
	await client.connect();
}

/** Disconnects the Discord client from the API. */
export function stopDiscord() {
	return client.disconnect({ reconnect: false });
}

client.on('ready', () => {
	logger.info("The client emitted the ready event. We're up.");
	client.editStatus('online', {
		name: `${prefix}help`,
		type: 0
	});
});

client.on('error', (err: Error) => {
	Logger.error('Error from Client: ', JSON.stringify(err));
});

client.on('messageCreate', (message: Message) => {
	const { author, channel, content } = message;

	// If it's a mention, they have no idea what the prefix is for some reason. Let's tell them.
	if (RegExp(`^\s*<@${client.user.id}>\s*$`).test(content)) {
		channel.createMessage(`The prefix is \`${prefix}\`. Try \`${prefix}help\` to learn more about rebot.`);
		return;
	}

	if (!content.startsWith(prefix) || author.bot) {
		return;
	}

	const inspected = inspectMessageContent(content);
	let command = getCommand(inspected.command);

	if (!command) {
		const msg = `Cannot find command "${inspected.command}".`;
		logger.debug(msg);
		if (nodeEnv === 'development') {
			channel.createMessage(msg);
		}

		return;
	}

	const { subCommands } = command;
	// Try and look for subcommands.
	if (subCommands) {
		const subCommandNames = Object.keys(subCommands);
		const currentSubCommandName = subCommandNames.find(name => inspected.args[0] === name);

		if (currentSubCommandName) {
			command = subCommands[currentSubCommandName];
		}

		
	}

	const { permissions, flags } = command;
	const parsedFlags = yargsParser(inspected.input, flags);

	if (permissions && !testPermissions(permissions, message.member!)) {
		channel.createMessage("You can't run this command.");
		return;
	}

	if (flags && flags.coerce) {
		const flagNames = Object.keys(parsedFlags).filter(x => x !== '_' && x !== '$0');
		const aliasNames = Object.keys(flags.alias || {});
		const coercionErrors = flagNames
			.filter(key => !aliasNames.includes(key) && (parsedFlags as any)[key] instanceof Error)
			.map(key => {
				// Unsafe cast is ok here, as we're sure it's a FlagCoercionError.
				const value = ((parsedFlags as any)[key] as unknown) as FlagCoercionError;
				return `Error parsing flag \`${key}\`: ${value.message}`;
			});

		2;

		if (coercionErrors.length !== 0) {
			const errorMessage = coercionErrors.join('\n');
			message.channel.createMessage(errorMessage);
			return;
		}
	}

	if (command.sources) {
		const thisSource = getSourceForMessage(message);

		// If this source isn't supported by the command...
		if (!command.sources.includes(thisSource)) {
			channel.createMessage(`You can only use this command in ${command.sources.map(getPluralSourceName).join(', ')}.`);

			return;
		}
	}

	// Block the use of NSFW commands in non-NSFW channels.
	if (command.nsfw && !(channel as TextChannel).nsfw) {
		channel.createMessage(
			"You can't use this command here, as it's a NSFW command. Try running it again in a NSFW channel."
		);
		return;
	}

	try {
		command(message, { ...inspected }, parsedFlags);
	} catch (commandRunError) {
		channel.createMessage({
			embed: {
				title: "Please, PLEASE, forgive my creators, for they have faultered, and I have thus failed.",
				description:
					"We've had a bit of an error here. Don't worry, this has been logged and will (probably) be fixed. In the meantime, don't do that again."
			}
		});
		logger.error(`Error running command "${inspected.command}": `, commandRunError);
	}
});

// This happens when Rebot joins a guild.
client.on('guildCreate', async guild => {
	try {
		const didLeave = false;

		// We can only send the welcome message if we haven't left.
		if (!didLeave) {
			const channel = (guild.systemChannelID ? guild.channels.get(guild.systemChannelID) : guild.channels.find(channel => channel.type === ChannelType.GuildText)) as TextChannel;

			if (!channel) {
				Logger.error(`Could not find default channel! (guildCreate).`);
				return;
			}

			try {
				await send({ embed: aboutEmbed }, channel);
			} catch (error) {
				// TODO: add error logging for this
			}
		}
	} catch (error) {
		// TODO: add error logging for this
	}
});

client.on('messageDelete', message => {
	// Is the message an uncached message? (isn't a complete Message object)
	const isUncached = (message as any).type != null;
	const { channel } = message;

	if (channel instanceof GuildChannel) {
		const { guild } = channel;
		const guildHasModLog = 2; // todo: this
		// if (guildHasModLog) {
		// 	modLogChannel.createMessage(blahBlah);
		// }
	}
});
