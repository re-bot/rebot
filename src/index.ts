/** @prettier */

/// <reference path="./typings/index.d.ts" />
import 'regenerator-runtime';
import 'dotenv/config';
import Logger from 'utilities/logging';
import chalk from 'chalk';
import { randomArrayItem, createSpinner, once } from 'utilities/general-util';
import { commandLoader } from 'utilities/commands';
import { prefix, nodeEnv, requiredEnvironmentalVariables, unixSocketPath } from 'r-constants';
import { startDiscord } from './discord';
import { createDBConnection } from 'utilities/database/main';

// eslint-disable-next-line no-require
const fortune = require('fortune-cookie') as string[];

// Send a ping every 10s to the keepalive parent, which shows this thread
// hasn't died.
function sendPing() {
	process.send!('heartbeat');
}

if (!unixSocketPath) {
	Logger.error(`The runtime directory, aka the path for the Unix socket, could not be determined. Please set "REBOT_RUNTIME_PATH".`);
	process.exit(0);
}

if (!(process as any).channel && nodeEnv !== 'development') {
	// The watcher should always be used in production.
	if (nodeEnv !== 'development') {
		Logger.error('You need to run the development entry, not this file.');
		process.exit(0);
	}
}

if ((process as any).channel) {
	setInterval(sendPing, 10000);
}

Logger.debug(`
	  _           _
 _ __ ___| |__   ___ | |_
| '__/ _ \\ '_ \\ / _ \\| __|
| | |  __/ |_) | (_) | |_ _
|_|  \\___|_.__/ \\___/ \\__(_)

Running version ${__version__ || 'unknown'} (dev).
${chalk.italic(`"${randomArrayItem(fortune)}"`)}
`);

if (nodeEnv === 'development') {
	Logger.warn(
		"You are in development mode. Things may break, and they probably will. If you're not developing rebot, do not use this."
	);
}

Logger.debug(`Relevant environmental variables:
 ${requiredEnvironmentalVariables.map(name => chalk.bold[name in process.env ? 'green' : 'red'](name)).join(', ')}`);

// Make sure we're running in a suitable environment.
if (parseInt(process.version.split('.')[0].slice(1)) < 10) {
	Logger.warn("rebot doesn't support node versions under v10. Proceed with care.");
}

namespace Bootstrap {
	/** Loads all the Discord commands (with ora graphics). */
	export async function loadAllCommands() {
		const spinner = createSpinner('Loading all commands...').start();

		// Print a newline so the next message doesn't go on the same line.
		process.stdout.write('\n');

		try {
			await commandLoader.loadAll(true);
			spinner.succeed('Loaded all commands...');
		} catch (loadingError) {
			spinner.fail('Failed to load commands.');
			Logger.error(loadingError);
		}
	}

	/** Starts the Discord client (with ora graphics). */
	export const startDiscordClient = once(async () => {
		const spinner = createSpinner('Starting the Discord client...').start();

		try {
			await startDiscord();

			spinner.succeed('Started the Discord client.');
			await Bootstrap.loadAllCommands();
		} catch (clientStartError) {
			spinner.fail('Failed to start the Discord client.');
			Logger.error(clientStartError);
		}
	});

	export async function startDatabase() {
		const spinner = createSpinner('Creating a connection to the database...').start();

		try {
			await createDBConnection();
			spinner.succeed('Created a connection to the database.');
		} catch (databaseError) {
			spinner.fail('Failed to create a connection to the database.');
			Logger.error(databaseError);
		}
	}
}

(async function() {
	Logger.info(`Your prefix is ${prefix}. Try ${chalk.bold(`${prefix}help`)}.`);
	await Promise.all([Bootstrap.startDiscordClient(), Bootstrap.startDatabase()]);
	Logger.info('All services have been bootstrapped.');
})();
