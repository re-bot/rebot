/** @prettier */

/** This file is used for the `babel-node` CLI, as it does not support the passing of a `.ts` file as the input.  */
/// <reference path="./typings/index.d.ts" />
require('v8-compile-cache');
require('ts-node/register');
require('tsconfig-paths/register');

const { version } = require('../package.json');
console.log('Starting Rebot in development mode from the development index...');

// We need to mimic Babel here, and set up the variables it handles.
global.__version__ = version;

// @ts-ignore
require('@babel/register')({
	cwd: __dirname,
	extensions: ['.ts', '.js'],
	...require('../babel.config')
});

// Now!
require('./index.ts');
