/** @prettier */

import { default as NekosLifeClient } from 'nekos.life';
import { DiscordCommand } from './all-types';
import { Client } from 'eris';
import { token } from 'r-constants';

/** The global commands registry. */
export const commands = new Map<string, DiscordCommand>();

/** The global `Client` instance. */
export const client = new Client(token);

export const nekosLifeClient = new NekosLifeClient();
