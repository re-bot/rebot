declare module 'x-error' {
    class XError<ExtendingObject extends object> extends Error {
        /** Set the code of this error. */
        setCode (code: number): this;
        /** Set the code of this error. */
        c (code: number): this;

        /** Set the message of this error. */
        setMessage (message: string): this;
        /** Set the message of this error. */
        m (message: string): this;

        /** Set the severity of this error. */
        setSeverity (severity: number): this;
        /** Set the severity of this error. */
        s (severity: number): this;

        /** Set the HTTP code of this error. */
        setHttpCode (httpCode: number): this;
        /** Set the HTTP code of this error. */
        hc (httpCode: number): this;

        /** Set the HTTP response code of this error. */
        setHttpResponse (httpResponse: number): this;
        /** Set the HTTP response code of this error. */
        hr (httpResponse: number): this;

        /** Extend this error object with another object. */
        extend (source: ExtendingObject): this;
        /** Extend this error object with another object. */
        ex (source: ExtendingObject): this;

        /** Adds a `_debug` property to this error that contains the arguments provided to this function. */
        debug (...debuggingInformation: unknown[]): this;
        /** Adds a `_debug` property to this error that contains the arguments provided to this function. */
        d (...debuggingInformation: unknown[]): this;

        /** Extend this new `XError` object with another object, and provide a message. */
        constructor (message: string, source: ExtendingObject);

        /** Extend this new `XError` object with another object, and provide a message, and provide an error code. */
        constructor (code: number, message: string, source?: ExtendingObject);

        /** Create a new `XError` with the provided message, and an error code. */
        constructor (code: number, message: string);

        /** Create a new `XError` instance with the provided message. */
        constructor (message: string);

        /** Create a new `XError`, copying over private properties from the provided `Error` object. */
        constructor (error: Error);
    }

    export = XError;
}