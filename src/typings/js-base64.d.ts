declare module 'js-base64' {
    export const Base64: {
        /** Encode a string to base64. */
        encode (input: string): string;

        /** Decode a string to base64. */
        decode (input: string): string;
    }
}