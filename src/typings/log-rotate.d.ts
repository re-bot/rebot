declare module 'log-rotate' {
    function rotate (filePath: string, callback: (err: unknown) => void): void;
    function rotate (filePath: string, options: Options, callback: (err: unknown) => void): void;

    export interface Options {
        /** Limit the number of rotated files */
        count?: number;

        /** Compress rotated files with gzip */
        compress?: boolean;
    }

    export default rotate;
}