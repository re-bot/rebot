declare module 'cowsay' {
    export type Mode = 
        | 'b'
        | 'd'
        | 'g'
        | 'p'
        | 's'
        | 't'
        | 'w'
        | 'y'

    export interface Options {
        /** What thw cow will say. */
        text: string;

        /** Template for a cow, get inspiration from `./cows` */
        cow?: string;

        /** ect the appearance of the cow's eyes, equivalent to cowsay -e */
        eyes?: string;

        /** The tongue is configurable similarly to the eyes through -T and tongue_string, equivalent to cowsay -T */
        tongue?: string;

        /** If it is specified, the given message will not be word-wrapped. equivalent to cowsay -n */
        wrap?: boolean;

        /** Specifies roughly where the message should be wrapped. equivalent to cowsay -W */
        wrapLength?: number;

        mode: Mode;
    }

    export function say (options: Options): string;
    export function think (options: Options): string;

    // jesus fucking christ
    export function whyIsThisSoFuckingLong (): void;

    /* A cow template. */ 
    export const ACKBAR: string;

    /* A cow template. */ 
    export const APERTURE_BLANK: string;

    /* A cow template. */ 
    export const APERTURE: string;

    /* A cow template. */ 
    export const ARMADILLO: string;

    /* A cow template. */ 
    export const ATAT: string;

    /* A cow template. */ 
    export const ATOM: string;

    /* A cow template. */ 
    export const AWESOME_FACE: string;

    /* A cow template. */ 
    export const BANANA: string;

    /* A cow template. */ 
    export const BEARFACE: string;

    /* A cow template. */ 
    export const BEAVIS_ZEN: string;

    /* A cow template. */ 
    export const BEES: string;

    /* A cow template. */ 
    export const BILL_THE_CAT: string;

    /* A cow template. */ 
    export const BIOHAZARD: string;

    /* A cow template. */ 
    export const BISHOP: string;

    /* A cow template. */ 
    export const BLACK_MESA: string;

    /* A cow template. */ 
    export const BONG: string;

    /* A cow template. */ 
    export const BOX: string;

    /* A cow template. */ 
    export const BROKEN_HEART: string;

    /* A cow template. */ 
    export const BUD_FROGS: string;

    /* A cow template. */ 
    export const BUNNY: string;

    /* A cow template. */ 
    export const C3PO: string;

    /* A cow template. */ 
    export const CAKE: string;

    /* A cow template. */ 
    export const CAKE_WITH_CANDLES: string;

    /* A cow template. */ 
    export const CAT2: string;

    /* A cow template. */ 
    export const CAT: string;

    /* A cow template. */ 
    export const CATFENCE: string;

    /* A cow template. */ 
    export const CHARIZARDVICE: string;

    /* A cow template. */ 
    export const CHARLIE: string;

    /* A cow template. */ 
    export const CHEESE: string;

    /* A cow template. */ 
    export const CHESSMEN: string;

    /* A cow template. */ 
    export const CHITO: string;

    /* A cow template. */ 
    export const CLAW_ARM: string;

    /* A cow template. */ 
    export const CLIPPY: string;

    /* A cow template. */ 
    export const COMPANION_CUBE: string;

    /* A cow template. */ 
    export const COWER: string;

    /* A cow template. */ 
    export const COWFEE: string;

    /* A cow template. */ 
    export const CTHULHU_MINI: string;

    /* A cow template. */ 
    export const CUBE: string;

    /* A cow template. */ 
    export const DAEMON: string;

    /* A cow template. */ 
    export const DALEK: string;

    /* A cow template. */ 
    export const DALEK_SHOOTING: string;

    /* A cow template. */ 
    export const DEFAULT: string;

    /* A cow template. */ 
    export const DOCKER_WHALE: string;

    /* A cow template. */ 
    export const DOGE: string;

    /* A cow template. */ 
    export const DOLPHIN: string;

    /* A cow template. */ 
    export const DRAGON_AND_COW: string;

    /* A cow template. */ 
    export const DRAGON: string;

    /* A cow template. */ 
    export const EBI_FURAI: string;

    /* A cow template. */ 
    export const ELEPHANT2: string;

    /* A cow template. */ 
    export const ELEPHANT: string;

    /* A cow template. */ 
    export const ELEPHANT_IN_SNAKE: string;

    /* A cow template. */ 
    export const EXPLOSION: string;

    /* A cow template. */ 
    export const EYES: string;

    /* A cow template. */ 
    export const FAT_BANANA: string;

    /* A cow template. */ 
    export const FAT_COW: string;

    /* A cow template. */ 
    export const FENCE: string;

    /* A cow template. */ 
    export const FIRE: string;

    /* A cow template. */ 
    export const FLAMING_SHEEP: string;

    /* A cow template. */ 
    export const FOX: string;

    /* A cow template. */ 
    export const GHOSTBUSTERS: string;

    /* A cow template. */ 
    export const GHOST: string;

    /* A cow template. */ 
    export const GLADOS: string;

    /* A cow template. */ 
    export const GOAT2: string;

    /* A cow template. */ 
    export const GOAT: string;

    /* A cow template. */ 
    export const GOLDEN_EAGLE: string;

    /* A cow template. */ 
    export const HAND: string;

    /* A cow template. */ 
    export const HAPPY_WHALE: string;

    /* A cow template. */ 
    export const HEDGEHOG: string;

    /* A cow template. */ 
    export const HELLOKITTY: string;

    /* A cow template. */ 
    export const HIPPIE: string;

    /* A cow template. */ 
    export const HIYA: string;

    /* A cow template. */ 
    export const HIYOKO: string;

    /* A cow template. */ 
    export const HOMER: string;

    /* A cow template. */ 
    export const HYPNO: string;

    /* A cow template. */ 
    export const IBM: string;

    /* A cow template. */ 
    export const IWASHI: string;

    /* A cow template. */ 
    export const JELLYFISH: string;

    /* A cow template. */ 
    export const KARL_MARX: string;

    /* A cow template. */ 
    export const KILROY: string;

    /* A cow template. */ 
    export const KING: string;

    /* A cow template. */ 
    export const KISS: string;

    /* A cow template. */ 
    export const KITTEN: string;

    /* A cow template. */ 
    export const KITTY: string;

    /* A cow template. */ 
    export const KNIGHT: string;

    /* A cow template. */ 
    export const KOALA: string;

    /* A cow template. */ 
    export const KOSH: string;

    /* A cow template. */ 
    export const LAMB2: string;

    /* A cow template. */ 
    export const LAMB: string;

    /* A cow template. */ 
    export const LIGHTBULB: string;

    /* A cow template. */ 
    export const LOBSTER: string;

    /* A cow template. */ 
    export const LOLLERSKATES: string;

    /* A cow template. */ 
    export const LUKE_KOALA: string;

    /* A cow template. */ 
    export const MAILCHIMP: string;

    /* A cow template. */ 
    export const MAZE_RUNNER: string;

    /* A cow template. */ 
    export const MECH_AND_COW: string;

    /* A cow template. */ 
    export const MEOW: string;

    /* A cow template. */ 
    export const MILK: string;

    /* A cow template. */ 
    export const MINOTAUR: string;

    /* A cow template. */ 
    export const MONA_LISA: string;

    /* A cow template. */ 
    export const MOOFASA: string;

    /* A cow template. */ 
    export const MOOGHIDJIRAH: string;

    /* A cow template. */ 
    export const MOOJIRA: string;

    /* A cow template. */ 
    export const MOOSE: string;

    /* A cow template. */ 
    export const MULE: string;

    /* A cow template. */ 
    export const MUTILATED: string;

    /* A cow template. */ 
    export const NYAN: string;

    /* A cow template. */ 
    export const OCTOPUS: string;

    /* A cow template. */ 
    export const OKAZU: string;

    /* A cow template. */ 
    export const OWL: string;

    /* A cow template. */ 
    export const PAWN: string;

    /* A cow template. */ 
    export const PERIODIC_TABLE: string;

    /* A cow template. */ 
    export const PERSONALITY_SPHERE: string;

    /* A cow template. */ 
    export const PINBALL_MACHINE: string;

    /* A cow template. */ 
    export const PSYCHIATRICHELP2: string;

    /* A cow template. */ 
    export const PSYCHIATRICHELP: string;

    /* A cow template. */ 
    export const PTERODACTYL: string;

    /* A cow template. */ 
    export const QUEEN: string;

    /* A cow template. */ 
    export const R2_D2: string;

    /* A cow template. */ 
    export const RADIO: string;

    /* A cow template. */ 
    export const REN: string;

    /* A cow template. */ 
    export const RENGE: string;

    /* A cow template. */ 
    export const ROBOT: string;

    /* A cow template. */ 
    export const ROBOTFINDSKITTEN: string;

    /* A cow template. */ 
    export const ROFLCOPTER: string;

    /* A cow template. */ 
    export const ROOK: string;

    /* A cow template. */ 
    export const SACHIKO: string;

    /* A cow template. */ 
    export const SATANIC: string;

    /* A cow template. */ 
    export const SEAHORSE_BIG: string;

    /* A cow template. */ 
    export const SEAHORSE: string;

    /* A cow template. */ 
    export const SHEEP: string;

    /* A cow template. */ 
    export const SHIKATO: string;

    /* A cow template. */ 
    export const SHRUG: string;

    /* A cow template. */ 
    export const SKELETON: string;

    /* A cow template. */ 
    export const SMALL: string;

    /* A cow template. */ 
    export const SMILING_OCTOPUS: string;

    /* A cow template. */ 
    export const SNOOPY: string;

    /* A cow template. */ 
    export const SNOOPYHOUSE: string;

    /* A cow template. */ 
    export const SNOOPYSLEEP: string;

    /* A cow template. */ 
    export const SPIDERCOW: string;

    /* A cow template. */ 
    export const SQUID: string;

    /* A cow template. */ 
    export const SQUIRREL: string;

    /* A cow template. */ 
    export const STEGOSAURUS: string;

    /* A cow template. */ 
    export const STIMPY: string;

    /* A cow template. */ 
    export const SUDOWOODO: string;

    /* A cow template. */ 
    export const SUPERMILKER: string;

    /* A cow template. */ 
    export const SURGERY: string;

    /* A cow template. */ 
    export const TABLEFLIP: string;

    /* A cow template. */ 
    export const TAXI: string;

    /* A cow template. */ 
    export const TELEBEARS: string;

    /* A cow template. */ 
    export const TEMPLATE: string;

    /* A cow template. */ 
    export const THREADER: string;

    /* A cow template. */ 
    export const THREECUBES: string;

    /* A cow template. */ 
    export const TOASTER: string;

    /* A cow template. */ 
    export const TORTOISE: string;

    /* A cow template. */ 
    export const TURKEY: string;

    /* A cow template. */ 
    export const TURTLE: string;

    /* A cow template. */ 
    export const TUX_BIG: string;

    /* A cow template. */ 
    export const TUX: string;

    /* A cow template. */ 
    export const TWEETY_BIRD: string;

    /* A cow template. */ 
    export const USA: string;

    /* A cow template. */ 
    export const VADER: string;

    /* A cow template. */ 
    export const VADER_KOALA: string;

    /* A cow template. */ 
    export const WEEPING_ANGEL: string;

    /* A cow template. */ 
    export const WHALE: string;

    /* A cow template. */ 
    export const WIZARD: string;

    /* A cow template. */ 
    export const WOOD: string;

    /* A cow template. */ 
    export const WORLD: string;

    /* A cow template. */ 
    export const WWW: string;

    /* A cow template. */ 
    export const YASUNA_01: string;

    /* A cow template. */ 
    export const YASUNA_02: string;

    /* A cow template. */ 
    export const YASUNA_03A: string;

    /* A cow template. */ 
    export const YASUNA_03: string;

    /* A cow template. */ 
    export const YASUNA_04: string;

    /* A cow template. */ 
    export const YASUNA_05: string;

    /* A cow template. */ 
    export const YASUNA_06: string;

    /* A cow template. */ 
    export const YASUNA_07: string;

    /* A cow template. */ 
    export const YASUNA_08: string;

    /* A cow template. */ 
    export const YASUNA_09: string;

    /* A cow template. */ 
    export const YASUNA_10: string;

    /* A cow template. */ 
    export const YASUNA_11: string;

    /* A cow template. */ 
    export const YASUNA_12: string;

    /* A cow template. */ 
    export const YASUNA_13: string;

    /* A cow template. */ 
    export const YASUNA_14: string;

    /* A cow template. */ 
    export const YASUNA_16: string;

    /* A cow template. */ 
    export const YASUNA_17: string;

    /* A cow template. */ 
    export const YASUNA_18: string;

    /* A cow template. */ 
    export const YASUNA_19: string;

    /* A cow template. */ 
    export const YASUNA_20: string;

    /* A cow template. */ 
    export const YMD_UDON: string;

    /* A cow template. */ 
    export const ZEN_NOH_MILK: string;

}