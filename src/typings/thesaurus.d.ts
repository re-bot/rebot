declare module 'thesaurus' {
	export type Database = Record<string, string[]>;

	class Thesaurus {
		/**
		 * Returns a list of similar words.
		 * Please be aware that all meanings are merged into the same list.
		 * This is not a semantic thesaurus.
		 */
		find(word: string): string[];

		/** Returns a stringified version of the database. */
		toJson(): string;

		/** Returns this `Thesaurus`, with the definitions from the file loaded. */
		load(path: string): this;

		/** Clears the database of definitions. */
		reset(): void;

		/** Gets the database used by this `Thesaurus` instance. */
		get(): Database;

		/** Replaces all definitions in the database with the ones in the file. */
		replace(path: string): this;
	}

	let thesaurus: Thesaurus;
	export default thesaurus;
}
