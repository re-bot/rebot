/** @prettier */

import { Message, GuildChannel } from 'eris';

/** Used to implement constraints for guild-only / dm-only commands. */
export enum Source {
	Guild = 'guild',
	DirectMessage = 'direct message'
}

/** A pluralized `sourceNames`. */
export const pluralSourceNames = {
	[Source.Guild]: 'guilds',
	[Source.DirectMessage]: 'direct messages'
};

/** Get the source of a message. */
export function getSourceForMessage(message: Message) {
	return (message.channel as GuildChannel).guild ? Source.Guild : Source.DirectMessage;
}

/** Get an incorrect source error for the expected source. */
export function getIncorrectSourceError(expected: Source) {
	return `Sorry. You can't use this command in a ${expected}.`;
}

/** Creates a string error that explains how none of the sources match. */
export function createErrorFromSources(sources: Source[]) {
	return `Sorry. You can only use this command in any one of these: \`${sources.join(', ')}\`.`;
}

export function getPluralSourceName(source: Source) {
	return pluralSourceNames[source];
}
