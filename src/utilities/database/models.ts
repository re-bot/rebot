/** @prettier */

import { BaseEntity, Entity, Column, CreateDateColumn, UpdateDateColumn, PrimaryColumn } from 'typeorm';
import { RebotPermission } from 'all-types';

@Entity()
export class Tag extends BaseEntity {
	@PrimaryColumn()
	name!: string;

	@Column()
	content!: string;

	@CreateDateColumn()
	createdAt!: string;

	@UpdateDateColumn()
	updatedAt!: string;

	@Column()
	authorID!: string;

	@Column({ nullable: true, type: 'text' })
	guildId!: string | null;
}

@Entity()
export class Poll extends BaseEntity {
	@PrimaryColumn()
	messageId!: string;

	@Column()
	channelId!: string;

	@CreateDateColumn()
	createdAt!: string;

	@UpdateDateColumn()
	updatedAt!: string;

	@Column()
	authorId!: string;

	@Column('simple-array')
	choices!: string[];
}

@Entity()
export class RebotGuild extends BaseEntity {
	@PrimaryColumn()
	id!: string;

	@Column('simple-json')
	settings!: string;
}
