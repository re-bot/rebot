/** @prettier */

import { createConnection } from 'typeorm';
import { nodeEnv } from 'r-constants';
import { Tag, Poll, RebotGuild } from './models';

export function createDBConnection() {
	return createConnection({
		type: 'postgres',
		host: 'localhost',
		port: 5432,
		username: 'rebot',
		password: 'rebot',
		database: 'rebot',
		synchronize: nodeEnv === 'development',
		logging: false,
		entities: [Tag, Poll, RebotGuild],
		cache: nodeEnv === 'production'
	});
}

