/** @prettier */

import { commands } from 'state';
import { BaseDiscordCommand, DiscordCommand, CommandOptions } from 'all-types';
import { commandMarker } from 'r-constants';
import { CommandLoader } from './module-loading/command-loader';

/** Gets the command from the global command registry. This is useful for functions that don't need to know about the global command registry (makes it easier for us to change, and hook this function). */
export function getCommand(commandName: string) {
	return commands.get(commandName);
}

/** Creates a `DiscordCommand` from a `BaseDiscordCommand`, taking an options object (`CommandOptions`). */
export function command(base: BaseDiscordCommand, options: CommandOptions = {}): DiscordCommand {
	// Create a clone of the function.
	function thru(this: ThisType<typeof base>, ...args: Parameters<typeof base>) {
		// eslint-disable-next-line no-invalid-this
		return base.call(this, ...args);
	}

	thru.displayName = (base as any).displayName || base.name;

	Object.assign(thru, options, base);

	if (!(thru as any).subCommands) {
		// We need this to exist.
		// @ts-ignore
		thru.subCommands = [];
	}

	// Mark the function wrapper as a command.
	(thru as any)[commandMarker] = true;

	// This can't return a proper DiscordCommand instance with "group" and "path" properties, as those are assigned by
	// the CommandLoader.
	// @ts-ignore
	return thru;
}

// todo: move to state
/** The global command loader. */
export const commandLoader = new CommandLoader();
