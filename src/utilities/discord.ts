/** @prettier */

import { User, Member, Guild, EmbedOptions, PossiblyUncachedMessage, Emoji, TextChannel, Message } from 'eris';
import { client } from 'state';
import { Permissions, UselessGuildCheck, RebotPermission, PaginatorDispatchOptions, CommandOptions, ResolvableColorName } from 'all-types';
import { owners, rebotServerID, nextEmoji, forwardEmoji, backwardEmoji, numbersEmoji, cancelEmoji, ChannelType, minimumMembers, uselessGuildCheckMessages } from 'r-constants';
// @ts-ignore
import { stripIndent } from 'common-tags';
import NekosLifeClient from 'nekos.life';
import Logger from 'js-logger';
import { send } from './messages';
import { command } from './commands';

const uselessGuildLogger = Logger.get('useless-guild');

/** Tells us whether or not Rebot has permission to ban a specific user. */
export function isAllowedToBan(user: Member) {
	const { guild } = user;
	const botMember = getGuildMember(guild, client.user);
	if (user.id === '570025582151204864') {
		// yep. resynth is immune (sorry)
		return false;
	}

	return botMember.permission.has('BAN_MEMBERS') && compareHighestRoles(user, botMember) === -1;
}

/** Tells us whether or not Rebot has permission to perform a specific action, and the client's highest role in that guild is higher than the user's. */
export function isAllowedToPerformAction(user: Member, action: string) {
	const { guild } = user;
	const botMember = getGuildMember(guild, client.user);
	return botMember.permission.has(action) && compareHighestRoles(user, botMember) === -1;
}

/** Gets the highest role a member has. */
export function getHighestRole(user: Member) {
	return user.roles
		.map(x => user.guild.roles.get(x))
		.reduce((acc, curr) => (curr!.position > acc!.position ? curr : acc));
}

/** Gets the `Member` instance (in the context of a specific guild) for a `User`. */
export function getGuildMember(guild: Guild, id: string | User) {
	const isID = typeof id === 'string';
	// @ts-ignore The next line breaks because TypeScript can't detect that the `id.id` bit means it's a User.
	return guild.members.find(x => (isID ? x.id === id : x.user.id === id.id));
}

/** Compares the highest roles of both users, using numbers, e.g
 * - `0` means they're equal,
 * - `1` means the first user has a higher role than the second,
 * - `-1` means the second user has a higher role than the first.
 */
export function compareHighestRoles(userOne: Member, userTwo: Member) {
	const first = getHighestRole(userOne)!.position;
	const second = getHighestRole(userTwo)!.position;
	return first < second ? -1 : first > second ? 1 : 0;
}

/** Tests if the message author has the requirements for the `Permissions` object. */
export function testPermissions(permissions: Permissions, author: Member) {
	if (permissions.roles && permissions.roles.every(role => author.roles.includes(role))) {
		return true;
	}

	if (permissions.nodes && permissions.nodes.every(node => author.permission.has(node))) {
		return true;
	}

	if (permissions.isOwnerCommand && owners.some(id => author.id === id)) {
		return true;
	}

	return false;
}

export function isEmptyGuild(guild: Guild) {
	/** All the non-bot members in the guild. */
	const userMembers = guild.members.filter(member => !member.bot);
	return userMembers.length <= 1;
}

/** Checks if the guild is classified as a "useless" guild, if it is, issue an informational message to the owner, then leave. */
export async function checkIfGuildIsUseless(guild: Guild) {
	if (guild.id === rebotServerID) {
		// I feel ashamed lmao
		return false;
	}

	const uselessGuildCheck = isUselessGuild(guild);
	const values = Object.values(uselessGuildCheck);

	if (values.every(item => item === false)) {
		// It's not a useless guild.
		return false;
	}

	const reasons = Object.keys(uselessGuildCheck)
		.map(key => `- ${uselessGuildCheckMessages[key as keyof UselessGuildCheck]}`)
		.join('\n');
	const guildOwner = getGuildMember(guild, guild.ownerID);
	const dmChannel = await guildOwner.user.getDMChannel();

	try {
		// Send the message before we leave, because this guild might be the only mutual one between the owner and Rebot.
		await dmChannel.createMessage(stripIndent`Sorry about this, but I'm going to have to leave your server, since it doesn't meet the minimum requirements:\n${reasons}\n
		If you `);
	} catch (error) {
		uselessGuildLogger.error(
			`Could not direct message owner with message (user id = ${guildOwner.id}, guild id = ${guild.id}): `,
			error
		);
	}

	try {
		// Don't message and leave in the same try block, as if the owner has Direct Messages disabled, the leave would never happen.
		await guild.leave();
	} catch (error) {
		uselessGuildLogger.error(`Could not perform leave (guild id = ${guild.id}): `, error);
		// Rethrow the error in the async context.
		throw error;
	}

	return true;
}

/** Checks if the guild is "useless", which means it only has under or equal to the minimum member count, and the bot cannot speak in any channels. */
export function isUselessGuild(guild: Guild): UselessGuildCheck {
	// Compute them all here so users that don't match either of these criteria won't have to keep adding the bot back if they don't fix them both at once.
	const hasAnyAvailableChannels = canSpeakInAnyChannels(guild);
	const hasMinimumMembers = guild.members.filter(member => !member.bot).length >= minimumMembers;

	return { hasAnyAvailableChannels, hasMinimumMembers };
}

/** Check if the bot can speak in any channels. */
export function canSpeakInAnyChannels(guild: Guild) {
	const clientMember = guild.members.find(member => member.id === client.user.id);
	const { channels } = guild;
	const availableChannels = channels.filter(
		channel => channel.type === ChannelType.GuildText && !channel.permissionsOf(clientMember.id).has('SEND_MESSAGES')
	);
	return availableChannels.length !== 0;
}

// /** Checks if the user ID has a special Rebot permission. */
// export async function userHasRebotPermission (userID: string, permission: RebotPermission) {
// 	// todo: make all async functions rethrow errors for simplicity
// 	const user = await RebotUser.findOne({ id: userID });
// 	return user?.permissions.includes(permission);
// }

// /** Grants a Rebot permission to a user. */
// export async function grantRebotPermission (userID: string, permission: RebotPermission) {
// 	const user = await RebotUser.findOne({ id: userID });
	
// 	if (!user?.permissions.includes(permission)) {
// 		user?.permissions.push(permission);
// 	}

// 	user?.save();
// 	return user;
// }

export abstract class Paginator {
	protected index = 0;

	/** The amount of time before the paginated embed is discarded, and any futher reactions are ignored. */
	protected timeout = 10000;

	/** The message ID of the paginated embed. */
	protected messageID!: string;

	/** The handler for the reaction add event. */
	protected onReactionAddHandler = (message: PossiblyUncachedMessage, emoji: Emoji, userID: string) => {
		if (this.messageID !== message.id || userID !== this.authorID) {
			return;
		}

		this.dispatch({ emoji });
	}

	constructor (public channel: TextChannel, public authorID: string) {
		
	}

	protected async dispatch ({ emoji }: PaginatorDispatchOptions) {
		switch (emoji.name) {
			case forwardEmoji: {
//				if (this.index >= this.)
			}
		}
	}

	protected async initialize () {
		const firstPage = this.getPage(0);
		const message = await send({ embed: firstPage }, this.channel);
		this.addReactions(message);
		this.messageID = message.id;
		client.on('messageReactionAdd', this.onReactionAddHandler);
	}

	/** Adds the reactions to the message. */
	protected async addReactions (message: Message) {
		const promises: Promise<void>[] = [];
		promises.push(message.addReaction(forwardEmoji));
		promises.push(message.addReaction(backwardEmoji));
		promises.push(message.addReaction(numbersEmoji));
		promises.push(message.addReaction(cancelEmoji));

		return Promise.all(promises);
	}

	protected update (index: number) {

	}

	abstract getPage (index: number): EmbedOptions;
}

export function createMathCommand(func: { (...args: number[]): number }, argsLength: number, options?: CommandOptions) {
	return command((message, inspected, flags) => {
		const { _: args } = flags;

		if (argsLength < args.length) {
			return send(`This command only requires ${argsLength} arguments.`, message);
		}

		const parsedArgs = args.map((arg: string) => parseFloat(arg));
		const result = func(...parsedArgs);

		return send(`\`${result}\``, message);
	}, options);
}

export function createBasicMathCommand (func: { (...args: string[]): number }, options?: CommandOptions) {
	return command((message, inspected, flags) => {
		const { _: args } = flags;
		return send(`\`${func(...args)}\``, message);
	});
}

export function createNekosLifeImageCommand (func: () => Promise<NekosLifeClient.NekoRequestResults>, commandOptions?: CommandOptions) {
	return command(async (message) => {
		const { url } = await func();

		return send({
			embed: {
				image: {
					url
				},
				url: 'https://nekos.life'
			}
		}, message);
	}, commandOptions);
}
