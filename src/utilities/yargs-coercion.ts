/** @prettier */

import { reChannelMention, reID, reYesNo } from 'r-constants';
import responseToBoolean from 'yn';

/** This class is an error returned from any of our coercion functions when the value cannot be coerced. */
export class FlagCoercionError extends TypeError {
	name = 'FlagCoercionError';
	// FIXME: should this be a POJO so a stack trace isn't collected, reducing mem usage?
	constructor(public expectedType: string) {
		super(`Expected the the flag to be a ${expectedType}.`);
	}
}

export function mention(channelMention: string) {
	// eslint-disable-next-line no-magic-numbers
	const resolved = reChannelMention.test(channelMention) ? channelMention.slice(2, -1) : channelMention;

	if (!reID.test(resolved)) {
		return new FlagCoercionError('valid mention');
	}

	return resolved;
}

export function boolean(response: string) {
	// Remove empty spaces from the input.
	response = response.replace(/^\s+|\s+$/, '');

	if (!reYesNo.test(response)) {
		return new FlagCoercionError('yes/no-like value');
	}

	return responseToBoolean(response);
}

export function cowsayMode(response: string) {
	return ['b', 'd', 'g', 'p', 's', 't', 'w', 'y'].includes(response.toLowerCase())
		? response
		: new FlagCoercionError('cowsay mode (b, d, g, p, s, t, w or y).');
}
