/** @prettier */

import figlet from 'figlet';

// eslint
export const allFigletFonts = figlet.fontsSync();

export const isFigletFont = (fontName: string) => allFigletFonts.some(fonts => fonts.includes(fontName));
