/** @prettier */

import Logger from 'js-logger';
import chalk from 'chalk';
import { format } from 'date-fns';
import rotatingFileStream from 'rotating-file-stream';
import { logFilePath } from 'r-constants';

/* We need this here, because of a (kind of) circular dependency problem Rebot has, I'll try to explain it here.
 ┌────────┐ ┌──────────────────────────┐ ┌──────────────┐ ┌───────────────────────┐
 │index.ts├─┤ Base/Environment/Install ├─┤ Base/Logging ├─┤ Constants/Environment │
 └────────┘ └──────────────────────────┘ └──────────────┘ └───────────────────────┘
 So basically, the environment constants file is required *before* the environment has been initialized. 
 This is a problem, so it's now this.
 ┌────────┐ ┌──────────────────────────┐ ┌──────────────┐
 │index.ts├─┤ Base/Environment/Install ├─┤ Base/Logging │
 └────────┘ └──────────────────────────┘ └──────────────┘
 This is all caused by one import. Such carnage!
*/
const nodeEnv = process.env.NODE_ENV;
Logger.useDefaults();

// Create a custom logging formatter for Rebot.
const handler = Logger.createDefaultHandler({
	formatter: (messages, logContext) => {
		// Ignore the "white" here. If this isn't here, TypeScript complains about it.
		let color: keyof typeof chalk = 'white';

		// Have special colors for each logging level, that show the logging level without explicitly prepending
		// it to the output.
		switch (logContext.level) {
			case Logger.ERROR:
				color = 'redBright';
				break;
			case Logger.WARN:
				color = 'yellow';
				break;
			case Logger.TRACE:
				color = 'grey';
				break;
		}

		// "white" is used here to shut TypeScript up.
		const formattedMessages = messages.map(message => chalk[color as 'white'](message));
		const date = new Date();
		const { name } = logContext;
		const timeStamp = format(date, 'H:m:ss');
		messages.splice(0, messages.length);
		messages.push(`[${chalk.gray(timeStamp)}${name ? ` ${name}` : ''}]`, ...formattedMessages);
	}
});

Logger.setHandler(handler);

// Automatically adjust the logging level for the environment.
if (nodeEnv === 'development') {
	Logger.setLevel(Logger.DEBUG);
} else {
	// This is set to "info", because "info" is used to signal important things to the user, like the emission of the
	// ready event, and initial login.
	Logger.setLevel(Logger.INFO);
}

export default Logger;


const logFileWriteStream = rotatingFileStream(logFilePath, {
	size: '10M',
	interval: '1d'
});

export function logError (error: unknown) {
	const date = new Date();
	const timeStamp = format(date, 'H:m:ss');
	// Instead of newlines, provide an indent to show that it's part of that log item.
	logFileWriteStream.write(`[${timeStamp} err]  ${String(error).replace('\n', '\n  ')}\n`);

	// Write to both the log stream and the console.
	Logger.error(error);
}

