/** @prettier */

import { Message, TextChannel, MessageContent, MessageFile, EmbedOptions, GuildChannel } from 'eris';
import { OperationResult, SendTarget, YargsParser, InspectedArguments, ResolvableColor, ResolvableColorName, EscapeMarkdownOptions, ClampStringOptions } from 'all-types';
import { prefix } from 'r-constants';
import yargsParser from 'yargs-parser';
import { OperationResultEmojis } from 'r-constants';

export function prependToMessage (message: MessageContent, contentToPrepend: string) {
	if (typeof message === 'object') {
		const { embed } = message;
		return {
			embed: {
				...embed,
				// prettier-ignore
				title: `${contentToPrepend}${embed!.title ?? ''}`
			}
		}
	};

	return `${contentToPrepend}${message}`;
}

/** Sends a message, taking an optional `OperationResult` parameter, which, when passed, will get translated into it's respective emoji, e.g `OperationResult.Success` would get mapped to a tick, and this is then prepended to the message. */
export async function send(
	messageContent: MessageContent,
	target: SendTarget,
	result?: OperationResult,
	file?: MessageFile
): Promise<Message> {
	if (typeof messageContent === 'string' && messageContent.length > 2000) {
		return send(
			{
				embed: {
					title: 'Please, PLEASE, forgive my creators, for they have faultered, and I have thus failed.',
					description: 'Rebot tried to send a message that\'s over 2000 characters.',
					footer: {
						text: 'This event has been logged.'
					}
				}
			},
			target
		);
	}

	const contentToPrepend = result ? `${OperationResultEmojis[result!]} ` : '';
	const wrappedMessageContent = prependToMessage(messageContent, contentToPrepend);

	if (target instanceof TextChannel) {
		return target.createMessage(wrappedMessageContent);
	} else {
		return target.channel.createMessage(wrappedMessageContent);
	}
}

/**
 * Escapes code block markdown in a string.
 * @param {string} text Content to escape
 */
export function escapeCodeBlock(text: string) {
	return text.replace(/```/g, '\\`\\`\\`');
}

/**
 * Escapes inline code markdown in a string.
 */
export function escapeInlineCode(text: string) {
	return text.replace(/(?<=^|[^`])`(?=[^`]|$)/g, '\\`');
}

/**
 * Escapes italic markdown in a string.
 */
export function escapeItalic(text: string) {
	let i = 0;
	text = text.replace(/(?<=^|[^*])\*([^*]|\*\*|$)/g, (_, match) => {
		if (match === '**') {
			return ++i % 2 ? `\\*${match}` : `${match}\\*`;
		}
		return `\\*${match}`;
	});
	i = 0;
	return text.replace(/(?<=^|[^_])_([^_]|__|$)/g, (_, match) => {
		if (match === '__') {
			return ++i % 2 ? `\\_${match}` : `${match}\\_`;
		}
		return `\\_${match}`;
	});
}

/**
 * Escapes bold markdown in a string.
 */
export function escapeBold(text: string) {
	let i = 0;
	return text.replace(/\*\*(\*)?/g, (_, match) => {
		if (match) {
			return ++i % 2 ? `${match}\\*\\*` : `\\*\\*${match}`;
		}
		return '\\*\\*';
	});
}

/**
 * Escapes underline markdown in a string.
 */
export function escapeUnderline(text: string) {
	let i = 0;
	return text.replace(/__(_)?/g, (_, match) => {
		if (match) {
			return ++i % 2 ? `${match}\\_\\_` : `\\_\\_${match}`;
		}
		return '\\_\\_';
	});
}

/**
 * Escapes strikethrough markdown in a string.
 */
export function escapeStrikethrough(text: string) {
	return text.replace(/~~/g, '\\~\\~');
}

/**
 * Escapes spoiler markdown in a string.
 */
export function escapeSpoiler(text: string) {
	return text.replace(/\|\|/g, '\\|\\|');
}

/** Escapes the Markdown in the text, depending on the passed configuration. By default, everything is escaped. */
export function escapeMarkdown(
	text: string,
	{
		codeBlock = true,
		inlineCode = true,
		bold = true,
		italic = true,
		underline = true,
		strikethrough = true,
		spoiler = true,
		codeBlockContent = true,
		inlineCodeContent = true
	}: EscapeMarkdownOptions = {}
): string {
	if (!codeBlockContent) {
		return text
			.split('```')
			.map((subString, index, array) => {
				if (index % 2 && index !== array.length - 1) {
					return subString;
				}
				return escapeMarkdown(subString, {
					inlineCode,
					bold,
					italic,
					underline,
					strikethrough,
					spoiler,
					inlineCodeContent
				});
			})
			.join(codeBlock ? '\\`\\`\\`' : '```');
	}
	if (!inlineCodeContent) {
		return text
			.split(/(?<=^|[^`])`(?=[^`]|$)/g)
			.map((subString, index, array) => {
				if (index % 2 && index !== array.length - 1) {
					return subString;
				}
				return escapeMarkdown(subString, {
					codeBlock,
					bold,
					italic,
					underline,
					strikethrough,
					spoiler
				});
			})
			.join(inlineCode ? '\\`' : '`');
	}

	// Apply all necessary transformations to the text.
	if (inlineCode) {
		text = escapeInlineCode(text);
	}

	if (codeBlock) {
		text = escapeCodeBlock(text);
	}

	if (italic) {
		text = escapeItalic(text);
	}

	if (bold) {
		text = escapeBold(text);
	}

	if (underline) {
		text = escapeUnderline(text);
	}

	if (strikethrough) {
		text = escapeStrikethrough(text);
	}

	if (spoiler) {
		text = escapeSpoiler(text);
	}

	return text;
}

/**
 * Resolves a ResolvableColor into a color number.
 */
export function resolveColor(color: string | ResolvableColor) {
	switch (color) {
		case ResolvableColorName.Random:
			return Math.floor(Math.random() * (0xffffff + 1));
		case ResolvableColorName.Default:
			return 0;
	}

	if (typeof color === 'string') {
		color = color || parseInt(color.replace('#', ''), 16);
	} else if (Array.isArray(color)) {
		color = (color[0] << 16) + (color[1] << 8) + color[2];
	}

	if (color < 0 || color > 0xffffff) {
		throw new RangeError('Error resolving color: Color Range');
	} else if (color && isNaN(color as never)) {
		throw new TypeError('Error resolving color: The color is NaN.');
	}

	return color;
}

/**
 * The content to put in a codeblock with all codeblock fences replaced by the equivalent backticks.
 * @param {string} text The string to be converted
 */
export function cleanCodeBlockContent(text: string) {
	return text.replace('```', '`\u200b``');
}

/**
 * This command does 6 things:
 *   - Gets the "base", which is the message with the prefix removed.
 *   - Gets the "split" variant, which is the base split by the spaces (/\s+/)
 *   - Gets the name of the command, e.g if the message is "!help", the command name would be "help"
 *   - Gets the arguments of the command, e.g if the message is "!help a b c", the arguments would be `["a", "b", "c"]`
 *   - Parses the message with `yargs-parser`. This still needs a good amount of work.
 */
export function inspectMessageContent(
	content: string,
	cmdPrefix: string = prefix,
	yargsOptions: YargsParser.Options = {}
): InspectedArguments {
	/** The command with the prefix removed. */
	const base = content.slice(cmdPrefix.length);

	/** A split version of the command, with the command name present at the beginning of the array. */
	const split = base.split(/\s+/);

	// The arguments passed to the command (without the command name or prefix present).
	// The name of the command, e.g "help".
	const [commandName, ...args] = split;

	// Remove the command name from the input.
	// TODO: should the input *not* include the flags?
	const input = base.slice(commandName.length).replace(/^\s/, '');

	return {
		base,
		split,
		command: commandName,
		args,
		input,
		flags: yargsParser(input.replace(/(`{3}(.|\n)+?`{3})|(`.+?`)/g, ''), yargsOptions)
	};
}

/** Clamps a string to a specific length. */
export function clampString (input: string, maxLength: number, { ellipsis = false }: ClampStringOptions) {
	let newString: string;
	if (input.length >= maxLength) {
		newString = input.slice(0, +maxLength);
		
		if (ellipsis) {
			return `${newString.slice(0, -3)}...`;
		}
	}

	return input;
}
