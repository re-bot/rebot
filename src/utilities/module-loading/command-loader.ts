/** @prettier */

import { ModuleLoader } from './module-loader';
import { DiscordCommand } from 'all-types';
import { commands } from 'state';
import { commandsPath } from 'r-constants';
import Logger from 'utilities/logging';

const commandLogger = Logger.get('command');

export class CommandLoader extends ModuleLoader<DiscordCommand> {
	constructor() {
		// Provide our registries / constants to the `ModuleLoader` constructor.
		super(commands, commandsPath, commandLogger);
	}

	register(name: string, command: DiscordCommand, { path, category }: { path?: string; category: string }) {
		// Some undefined commands seem to be leaking through.
		if (command == null) {
			return;
		}

		// Mark the command as NSFW if it's in the NSFW category, but don't change it if it's already marked as a NSFW
		// command.
		command.nsfw = 'nsfw' in command ? command.nsfw : /nsfw/i.test(category);
		return super.register(name, command, { path, category });
	}

	// TODO: clean this up, make the whole method more explicit, which won't need this duplicated code
	registerExports(
		mod: Record<string, DiscordCommand>,
		{ fileName, path, category }: { fileName: string; path: string; category: string }
	) {
		const mainMod = mod.default;

		this.register(fileName, mainMod, { path, category });

		for (const key of Object.keys(mod)) {
			if (key !== 'default') {
				this.logger.debug(`Registered subcommand ${key} of command ${fileName}`);
				mainMod.subCommands[key] = mod[key];
			}
		}
	}
}
