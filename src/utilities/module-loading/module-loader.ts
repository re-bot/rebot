/** @prettier */

import { parse, resolve } from 'path';
import Logger from 'utilities/logging';
import glob from 'fast-glob';
import chalk from 'chalk';
import { getLastDirectory } from 'utilities/general-util';

/** The general concept of this class is purely to load modules of any nature from a folder. It's an abstract
 * implementation, meaning it can be used for any value (see `Value` and `CommandLoader`). */
export class ModuleLoader<Value extends { path?: string; category?: string; modName?: string }> {
	/** All the categories this ModuleLoader has registered. */
	public categories: Set<string> = new Set();

	// eslint-disable-next-line no-useless-constructor no-empty-function
	constructor(public store: Map<string, Value>, public directory: string, public logger = Logger.get('modload')) {}

	/* eslint-disable class-methods-use-this */
	/** Asynchronously loads the module from the path. */
	require(path: string) {
		return import(path);
	}

	/** Gets the name of the mod via it's path. */
	getNameFromPath(path: string) {
		return parse(path).name;
	}

	/* eslint-enable class-methods-use-this */

	/** Creates a path to a mod file, taking a name. */
	createPathFromName(name: string) {
		return resolve(this.directory, name);
	}

	/** Loads the mod by it's name. */
	loadFromName(name: string) {
		const path = this.createPathFromName(name);
		return this.loadFromPath(path);
	}

	/** Resolves the path relative to the current directory. */
	resolvePath(path: string) {
		return resolve(this.directory, path);
	}

	/** Loads the mod by it's path. */
	async loadFromPath(path: string) {
		try {
			const mod = (await this.require(this.resolvePath(path))) as Record<string, Value>;
			const fileName = this.getNameFromPath(path);
			const category = getLastDirectory(path);

			this.registerExports(mod, { fileName, path, category });
			this.logger.debug(`Loaded mod ${chalk.bold(this.getNameFromPath(path))} with category ${category}.`);
		} catch (loadError) {
			this.logger.error(`Failed to import path "${path}". Are you sure that's a valid path? `, loadError);
			// Rethrow the error in the asynchronous context.
			throw loadError;
		}
	}

	/** Registers all the exports from a module into the registry. */
	registerExports(
		mod: Record<string, Value>,
		{ fileName, path, category }: { fileName: string; path: string; category: string }
	) {
		for (const key of Object.keys(mod)) {
			if (key === 'default') {
				this.register(fileName, mod[key], { path, category });
				continue;
			}

			this.logger.debug(`Loaded mod ${key}.`);
			this.register(key, mod[key], { path, category });
		}
	}

	// TODO: move inline interface to RegisterOptions
	/** Adds a command to the registry. */
	register(name: string, value: Value, { path, category }: { path?: string; category: string }) {
		if (this.store.has(name)) {
			this.logger.warn(`Duplicate register call for mod ${name}.`);
		}

		if (path) {
			value.path = path;
		}

		value.modName = name;
		value.category = category;
		this.categories.add(category);
		this.store.set(name, value);
	}

	/** Gets all the files from the directory. */
	getAllFilesInDirectory (recursive = true) {
		return glob(resolve(this.directory, recursive ? './**/*.[tj]s' : './*.[tj]s'));
	}

	/** Loads all mods from the directory. */
	async loadAll(recursive = true) {
		const files = await this.getAllFilesInDirectory(recursive);

		const promises: Array<Promise<unknown>> = [];

		for (const file of files) {
			promises.push(this.loadFromPath(file));
			this.logger.debug(`Loading modules from file ${chalk.bold(file)}`);
		}

		return Promise.all(promises);
	}

	async reloadByName(name: string) {
		const command = this.store.get(name);

		if (!command) {
			this.logger.error(`Cannot reload module ${name}, as it cannot be found in the registry.`);
			return;
		}

		this.unloadByName(name);
		await this.loadFromPath(command.path!);
	}

	/** Uncache and removes the command from the registry. */
	unloadByName(name: string) {
		if (!this.store.has(name)) {
			this.logger.warn(`Cannot unload command "${name}". It isn't in the registry.`);
			return;
		}

		this.uncacheByName(name);
	}

	/** Removes the mod from the `require` cache. */
	uncacheByName(name: string) {
		if (!this.store.has(name)) {
			this.logger.error(`Cannot uncache command "${name}". It isn't in the registry.`);
		}

		const mod = this.store.get(name)!;

		if (!mod.path) {
			this.logger.error(`Cannot uncache command "${name}". It doesn't have a "path" property.`);
		}

		try {
			delete require.cache[require.resolve(this.createPathFromName(mod.path!))];

			this.logger.debug(`Uncached mod ${name}.`);
		} catch (uncacheError) {
			this.logger.error(`Failed to uncache mod ${name}. `, uncacheError);
		}
	}
}
