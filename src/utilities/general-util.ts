/** @prettier */

import ora from 'ora';
import { parse } from 'path';

/** Gets a random item from the array. */
export function randomArrayItem<ArrayItem>(array: ArrayItem[]): ArrayItem {
	return array[Math.floor(Math.random() * array.length)];
}

/** Transforms an Array into an array of numbered items, e.g `['1. hello', '2. world']` */
export function toNumberedList<Item>(list: Item[]): string[] {
	return list.map((value, index) => `${index}. ${value}`);
}

/** Creates a lazily accessible value. Returns a function that, when called, calls the factory (on the first call), then returns the cached return value of the factory for any subsequent calls. */
export function Lazy<Value>(factory: () => Value) {
	let accessed = false;
	let value: Value | void;

	return function(): Value {
		if (accessed) {
			return value as Value;
		}

		accessed = true;
		value = factory();
		return value;
	};
}

export function attempt<Func extends (...args: unknown[]) => unknown>(
	this: ThisType<Func> | void,
	func: Func,
	...args: Parameters<Func>
) {
	try {
		return func.call(this, ...args);
	} catch (err) {
		return err;
	}
}

export function createSpinner(text: string | ora.Options) {
	const baseOptions: ora.Options = { spinner: 'dots5' };
	const options: ora.Options = typeof text === 'string' ? { text } : {};
	return ora(Object.assign(options, baseOptions));
}

/** Get the last directory from the path. */
export function getLastDirectory(path: string) {
	const parsed = parse(path);
	const dirs = parsed.dir.split('/');
	return dirs[dirs.length - 1];
}

/** Alias so the code makes more sense when `Lazy` isn't used for laziness. */
export const once = Lazy;

/** Runtime version of the "penv.macro" Babel macro. */
export function env<Value>(envMap: Record<string, Value>) {
	// eslint-disable-next-line no-process-env
	return envMap[process.env.NODE_ENV as string] || null;
}

/** Returns a Promise that is resolved with the `amountOfMs` after `amountOfMs` has elapsed. */
export function sleep(amountOfMs: number) {
	return new Promise(resolve => {
		setTimeout(resolve, amountOfMs);
	});
}

/** Creates an iterator that yields the current index from `start` to `end`, incrementing each time with `step`. */
export function* range(start: number, end: number, step = 1) {
	let index = start;

	while (index <= end) {
		yield index;
		index += step;
	}
}

const _hasOwnProperty = Object.prototype.hasOwnProperty;

/** Checks if the object has the specified key. This is safer than directly calling the `hasOwnProperty` method on the object, as the object may have a null prototype. */
export function hasOwnProperty(object: object, key: string) {
	return _hasOwnProperty.call(object, key);
}

/** Gets the fastest available `EventEmitter` implementation. */
export const getEventEmitterImpl = once((): (typeof import('eventemitter3')) => {
	try {
		// "eventemitter3" is marked as an optional dependency. We must respect that here.
		return require('eventemitter3');
	} catch {
		return require('events');
	}
});

/** The fastest available `EventEmitter` implementation. */
export const EventEmitter = getEventEmitterImpl();

