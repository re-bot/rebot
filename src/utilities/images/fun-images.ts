/** @prettier */

/** Gets a random URL of a Birb image. */
export function getRandomBirbImageURL() {
	const fileName = `Birb%20(${Math.floor(Math.random() * 588) + 1})`;
	const fileType = '.jpg';
	return `http://electrohaxz.tk/media/img/birbs/${fileName}${fileType}`;
}

/** Gets a random URL of something cute, as an image. */
export function getRandomCuteImageURL() {
	const fileName = `Cute%20(${Math.floor(Math.random() * 441) + 1})`;
	const fileType = '.jpg';
	return `http://electrohaxz.tk/media/img/cute/${fileName}${fileType}`;
}

/** Gets a random URL of a headpat image. */
export function getRandomHeadpatImageURL() {
	const fileName = `Headpat%20(${Math.floor(Math.random() * 910) + 1})`;
	const fileType = '.jpg';
	return `http://electrohaxz.tk/media/img/headpats/${fileName}${fileType}`;
}

/** Gets a random URL of a Neko image. */
export function getRandomNekoImageURL() {
	const fileName = `neko%20(${Math.floor(Math.random() * 441) + 1})`;
	const fileType = '.jpeg';
	return `http://electrohaxz.tk/media/img/anime-images/kawaii-nekos/${fileName}${fileType}`;
}
