/** @prettier */

/* This file is required from the main file. We need to make this file because Babel moves all non-import code *below* the imports, so we can't do this:
      import { installFileSync } from './Parser';
      installFileSync();
      // other imports

      As it would end up like this:
      import { installFileSync } from './Parser';
      // other imports
      installFileSync();

 Following this, we need to load this *before* any other core modules, as they may depend on environmental variables fetched from "Constants/Environment".
*/

import { installFileSync } from './parser';
installFileSync();
