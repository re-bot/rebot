/** @prettier */

import { promises, readFileSync } from 'fs';
import { DotEnvOptions } from 'all-types';
import { resolve } from 'path';
import Logger from 'utilities/logging';
const { readFile } = promises;

const logger = Logger.get('dotenv');

/** Bisects the line, revealing the key & value pair in an Array. */
function bisectLine(line: string) {
	return line.split('=').slice(0, 2);
}

/** Checks if the line is a comment.*/
function isComment(line: string) {
	return /^(#|\/\/)/.test(line);
}

/** Dequotes a vavue, and removes any spaces if quotes aren't present. */
function normalizeValue(value: string) {
	if (/^"(.+)"$/.test(value)) {
		// Drop the quotes.
		return value.slice(1, -1);
	}

	// There aren't any quotes there. Drop the spaces.
	return value.replace(/^\s+/, '');
}

/** Parses the source of a `.env` file into an object. */
export function parse(text: string) {
	let lineNumber = 0;
	const store: Record<string, string> = {};

	for (const line of text.split('\n')) {
		++lineNumber;

		if (/^\s+$/.test(line)) {
			logger.debug(`Found an empty line at line ${lineNumber}. Skipping.`);
			// It's an empty line.
			continue;
		}

		// Now we've made sure the line is 100% correct, we can parse it.
		const bisected = bisectLine(line);
		if (!isComment(line) && bisected.length < 2) {
			logger.error(
				`Syntax error on line number ${lineNumber}:\n  ${line}\n Expected either a comment, or a KEY=VALUE pair.`
			);

			// This fixes the return signature. Oops!
			return store;
		}

		const [key, value] = bisected;
		store[key] = normalizeValue(value);
	}

	
	return store;
}

/** Parses the content from a `.env` file. */
export async function parseFile(path: string, options?: { encoding?: string | null; flag?: string | number } | null) {
	const content = await readFile(path, options);
	return parse(content.toString());
}

/** Like `parseFile`, but runs synchronously. */
export function parseFileSync(path: string, options?: { encoding?: string | null; flag?: string } | null) {
	const content = readFileSync(path, options);
	return parse(content.toString());
}

/** Safely assings the object to `process.env`, skipping over any already-set values. */
export function install(record: Record<string, string>) {
	for (const varName of Object.keys(record)) {
		if (process.env[varName]) {
			logger.info(`Not writing environmental variable ${varName}: It is already present in process.env.`);
			continue;
		}

		process.env[varName] = record[varName as keyof typeof record];
	}
}

/** Takes an optional path (which defaults to the .env file in the cwd), and adds all the environmental variables from
 * that file to `process.env`. */
export async function installFile({ path: basePath, encoding: baseEncoding }: DotEnvOptions = {}) {
	const path = basePath || resolve(process.cwd(), '.env');
	const encoding = baseEncoding || 'utf8';

	const record = await parseFile(path, { encoding });
	install(record);
	return record;
}

/** A synchronous version of `installFile`. */
export function installFileSync({ path: basePath, encoding: baseEncoding }: DotEnvOptions = {}) {
	const path = basePath || resolve(process.cwd(), '.env');
	const encoding = baseEncoding || 'utf8';

	const record = parseFileSync(path, { encoding });
	install(record);
	return record;
}
