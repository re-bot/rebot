# Todo

1. [ ] AFK Command
2. [ ] RSS (and other) feeds
3. [ ] Word blacklist feature
4. [ ] Show past deleted messages command
5. [ ] Add an enable/disable feature for all commands on a per server and per channel basis
6. [ ] Custom commands so users can make their own commands that do things, but the interface must be simple so no coding is required (think block coding like scratch)
7. [ ] Welcome feature to welcome users and display a custom message in a channel you set welcoming them.
8. [ ] Stats on everything because data is beautiful
9. [ ] Entertainment features such as jokes, rock paper scissors, coin flip, dice roll, slots, gambling, memes, quizzes, etc
10. [ ] Would you rather command that gives a question and 2 answers, answer by clicking reactions.
11. [ ] Image templates that users can put custom text in, useful for roleplaying or joking
12. [ ] QR code maker command (**qr {link})
13. [ ] Photo editing features such as blur, pixelate, edge detection, emboss, smoothing, sharpen, filters, rotate, mirror, contrast, brightness, exposure, color, tint, etc
14. [ ] Feature to link and unlink channels, even across servers
15. [ ] User commands such as profile, avatar, xp, roles, permissions, ID, etc
16. [ ] Map command that will fetch you a map of whatever place you enter
17. [ ] Weather command to give weather about whatever place you enter
18. [ ] Tts feature to convert text to speech and send it in a voice channel
19. [ ] **PARTIALLY** ascii and figlet commands to convert images to ascii and text to figlets
20. [ ] Game profile integration for many popular games to see stats of a player.
21. [ ] AI based chat so you can talk to the bot and it will talk back based on AI, not based on preset answers
22. [ ] anime search on various sites
23. [ ] Command to change prefix on a per server basis
24. [ ] Vote link that opens a page which has a link that opens all the sites it's listed on. View http://ehxz.tk/hexvote as an example.
25. [x] Encode and decode command to encode things to base64, binary, hex, or others
26. [ ] Command to get an invite link for the bot
27. [x] Say command
28. [ ] Spotify command to view spotify info of a user, song, or playlist
29. [ ] Pandora command to view info of a pandora user
30. ~~[ ] Nickname set command~~
31. [ ] Moderation stuff (warnings, bans, mute, unmute, add and remove roles, purge, tempban, anti-raid, lockdown, antispam, anti-mention, role, automod, etc)
32. [ ] A good music bot based on Lavalink with all the typical features and playlists as well.
33. [ ] NSFW commands if you wanted, self explanatory
34. [ ] Command for every feature that the nekos.life API supports
35. [ ] **NEARLY**: Poll commands to create, edit, delete, and answer polls
36. [ ] Roleplay commands (economy, virtual items, buy, sell, eat, drink, date, marry, divorce, give items, work, jobs, daily activities, loot from messages, markets, etc)
37. [ ] Image fetch commands (cat, dog, snake, nature, other common stuff etc)
38. [ ] Furry commands (like idea #36 but with furry things)
39. [ ] Reminders (ex. `**remind {link or message} {time})
40. [ ] DuckDuckGo search command that gives the top result and sends it in chat
41. [ ] Feature to log everything that happens in the server to a channel for staff
42. [ ] Personal pin board per user or server. (ex. .pin {messageid} OR .pin {message})
43. [ ] Stats for commands used the most, daily, weekly, per server, and total/all time.
44. [ ] Stat command to see info about a user or server.
45. [ ] About command to see what the bot is about/for, what it can do, and why people would want it.
46. [ ] Suggestion command (`**suggest {idea}`) to suggest things which will then send them all in a channel in this server for you to look at. It should also have a feature so that when you approve a suggestion, you can use (`**suggestion {suggestion_ID}` approve/deny) and it will react to the original suggestion with either a green check or a red X
47. [x] Tag command to do what your bot used to do in the first place, save and retrieve text snippets
48. [ ] Command to upload and download a file with a 5 minute time limit and max size of 250mb so you can quickly upload and download files from one machine to another (ex. **upload image.png ran by the user would make the bot send Uploaded! File will expire in 5 minutes!) Download: `{link here}`
49. [ ] Command to test an app on a VPS, for example running **testapp {app.apk} would launch an Android VPS with that app installed and opened then send a link to view or use that app for 60 seconds in Discord along with an instance ID for log checking. Same with Windows. Example: **testapp {windowsapp.exe} would launch a Windows VPS with that app running then send a link to access that VPS for 60 seconds in chat along with an instance ID for log checking. It should also monitor the activity of that app so you can run a command like **applog {id of instance} to see exactly what that app did in terms of resource usage (average, min, and max) and what files it viewed, accessed, edited, etc and all network activity that app performed while running.
     [ ] Command to run code (js, python, html, etc) in a virtual environment such as a VPS for a limited amount of
      time (max 60 secs maybe) and send the output of it in chat, like exec but not on your actual VPS and also not any
      command, obviously block bad things like dd.
      > The last 2 would be super useful because then you could safely run
      > files that may be malicious without any worry or need to set up a VM or whatever
50. [ ] Also maybe make some commands paid for more time, an example would be that you could buy premium status on the bot to get access to priority support and longer run times on apps in virtual environments so instead of 60 seconds it would be 120-300 seconds. Not as a moneygrab like mee6 but just because it does cost money to run things like that.
51. [ ] Command to see times all over the world, for example **time Chicago would return the time in Chicago. As well as a command like my highnoon command in UwUminati
52. [ ] Add a currency command set to Rebot that has guild-specific currency (not global)
53. [ ] Speedtest command based on fast.com API (many npm modules available for that)
54. [ ] Speedtest command based on my speedtest for most accurate results (requires more effort)
55. [ ] Fix permissions
56. [ ] Add a diagnose command that looks for common issues about the Rebot setup, like broken command loading, api issues etc
57. [ ] Add everything here: <https://github.com/Nekos-life/nekos-dot-life>
58. [ ] Add the cool stuff from UwUminati: <https://hasteb.in/xojudeva.coffeescript>
59. [ ] Add Nyan Cat commands
60. [ ] Add annoy frank command
61. [ ] MDN search command
62. [ ] caniuse command